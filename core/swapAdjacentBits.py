import unittest


def chunks(l, n=2):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def swapAdjacentBits(n):
    b = format(n, '032b')
    return int(''.join([y + x for x, y in chunks(b)]), 2)


class MyTest(unittest.TestCase):

    def test01(self):
        n = 13
        y = 14
        self.assertEqual(swapAdjacentBits(n), y)

    def test02(self):
        n = 74
        y = 133
        self.assertEqual(swapAdjacentBits(n), y)

    def test03(self):
        n = 1073741823
        y = 1073741823
        self.assertEqual(swapAdjacentBits(n), y)

    def test04(self):
        n = 0
        y = 0
        self.assertEqual(swapAdjacentBits(n), y)

    def test05(self):
        n = 1
        y = 2
        self.assertEqual(swapAdjacentBits(n), y)

    def test06(self):
        n = 83748
        y = 166680
        self.assertEqual(swapAdjacentBits(n), y)


if __name__ == '__main__':
    unittest.main()
