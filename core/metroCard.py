from calendar import monthrange
from itertools import zip_longest
import unittest


def getdays(i):
    return monthrange(2017, i)[1]


def metroCard(n):
    days = [getdays(i) for i in range(1, 13)]
    z = zip_longest(days, days[1:], fillvalue=days[0])
    return sorted({y for x, y in z if x == n})


class MyTest(unittest.TestCase):

    def test01(self):
        n = 30
        y = [31]
        self.assertEqual(metroCard(n), y)
        pass

    def test02(self):
        n = 31
        y = [28, 30, 31]
        self.assertEqual(metroCard(n), y)


if __name__ == '__main__':
    unittest.main()
