from math import gcd
import unittest


def countBlackCells(n, m):
    if n == m:
        return n + 2 * (n - 1)
    if n ==1 or m == 1:
        return n * m
    return n + m - gcd(n, m) + 2 * (gcd(n, m) - 1)


class MyTest(unittest.TestCase):

    def test01(self):
        n, m = 3, 4
        y = 6
        self.assertEqual(countBlackCells(n, m), y)

    def test02(self):
        n, m = 3, 3
        y = 7
        self.assertEqual(countBlackCells(n, m), y)

    def test03(self):
        n, m = 2, 5
        y = 6
        self.assertEqual(countBlackCells(n, m), y)

    def test04(self):
        n, m = 1, 1
        y = 1
        self.assertEqual(countBlackCells(n, m), y)

    def test05(self):
        n, m = 1, 2
        y = 2
        self.assertEqual(countBlackCells(n, m), y)

    def test06(self):
        n, m = 1, 3
        y = 3
        self.assertEqual(countBlackCells(n, m), y)

    def test07(self):
        n, m = 1, 239
        y = 239
        self.assertEqual(countBlackCells(n, m), y)

    def test08(self):
        n, m = 33, 44
        y = 86
        self.assertEqual(countBlackCells(n, m), y)

    def test09(self):
        n, m = 16, 8
        y = 30
        self.assertEqual(countBlackCells(n, m), y)

    def test10(self):
        n, m = 66666, 88888
        y = 177774
        self.assertEqual(countBlackCells(n, m), y)


if __name__ == '__main__':
    unittest.main()
