import unittest


def arithmeticExpression(a, b, c):
    return c in [a + b, a - b, a * b, a / b]


class MyTest(unittest.TestCase):

    def test01(self):
        a, b, c = 2, 3, 5
        y = True
        self.assertEqual(arithmeticExpression(a, b, c), y)

    def test02(self):
        a, b, c = 8, 2, 4
        y = True
        self.assertEqual(arithmeticExpression(a, b, c), y)

    def test03(self):
        a, b, c = 8, 3, 2
        y = False
        self.assertEqual(arithmeticExpression(a, b, c), y)

    def test04(self):
        a, b, c = 6, 3, 3
        y = True
        self.assertEqual(arithmeticExpression(a, b, c), y)

    def test05(self):
        a, b, c = 18, 2, 9
        y = True
        self.assertEqual(arithmeticExpression(a, b, c), y)

    def test06(self):
        a, b, c = 2, 3, 6
        y = True
        self.assertEqual(arithmeticExpression(a, b, c), y)

    def test07(self):
        a, b, c = 5, 2, 0
        y = False
        self.assertEqual(arithmeticExpression(a, b, c), y)

    def test08(self):
        a, b, c = 10, 2, 2
        y = False
        self.assertEqual(arithmeticExpression(a, b, c), y)

    def test09(self):
        a, b, c = 5, 2, 2
        y = False
        self.assertEqual(arithmeticExpression(a, b, c), y)

    def test10(self):
        a, b, c = 1, 2, 1
        y = False
        self.assertEqual(arithmeticExpression(a, b, c), y)


if __name__ == '__main__':
    unittest.main()
