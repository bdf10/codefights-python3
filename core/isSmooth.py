import unittest


def middle(l):
    n = len(l)
    m = n // 2
    if n % 2:
        return l[m]
    return l[m] + l[m - 1]


def isSmooth(l):
    return l[0] == l[-1] == middle(l)


class MyTest(unittest.TestCase):

    def test01(self):
        l = [7, 2, 2, 5, 10, 7]
        y = True
        self.assertEqual(isSmooth(l), y)

    def test02(self):
        l = [-5, -5, 10]
        y = False
        self.assertEqual(isSmooth(l), y)

    def test03(self):
        l = [4, 2]
        y = False
        self.assertEqual(isSmooth(l), y)

    def test04(self):
        l = [45, 23, 12, 33, 12, 453, -234, -45]
        y = False
        self.assertEqual(isSmooth(l), y)

    def test05(self):
        l = [-12, 34, 40, -5, -12, 4, 0, 0, -12]
        y = True
        self.assertEqual(isSmooth(l), y)

    def test06(self):
        l = [9, 0, 15, 9]
        y = False
        self.assertEqual(isSmooth(l), y)

    def test07(self):
        l = [-6, 6, -6]
        y = False
        self.assertEqual(isSmooth(l), y)

    def test08(self):
        l = [26, 26, -17]
        y = False
        self.assertEqual(isSmooth(l), y)

    def test09(self):
        l = [-7, 5, 5, 10]
        y = False
        self.assertEqual(isSmooth(l), y)

    def test10(self):
        l = [3, 4, 5]
        y = False
        self.assertEqual(isSmooth(l), y)

    def test11(self):
        l = [-5, 3, -1, 9]
        y = False
        self.assertEqual(isSmooth(l), y)


if __name__ == '__main__':
    unittest.main()
