import unittest


def additionWithoutCarrying(m, n):
    maxlen = max(map(lambda x: len(str(x)), (m, n)))
    sm, sn = map(lambda x: str(x).zfill(maxlen), (m, n))
    digits = map(lambda t: sum(int(x) for x in t) % 10, zip(sm, sn))
    return int(''.join(str(digit) for digit in digits))


class MyTest(unittest.TestCase):

    def test01(self):
        m, n = 456, 1734
        y = 1180
        self.assertEqual(additionWithoutCarrying(m, n), y)

    def test02(self):
        m, n = 99999, 0
        y = 99999
        self.assertEqual(additionWithoutCarrying(m, n), y)

    def test03(self):
        m, n = 999, 999
        y = 888
        self.assertEqual(additionWithoutCarrying(m, n), y)

    def test04(self):
        m, n = 0, 0
        y = 0
        self.assertEqual(additionWithoutCarrying(m, n), y)

    def test05(self):
        m, n = 54321, 54321
        y = 8642
        self.assertEqual(additionWithoutCarrying(m, n), y)


if __name__ == '__main__':
    unittest.main()
