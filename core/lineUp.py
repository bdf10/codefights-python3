import unittest


def left(v):
    x, y = v
    return -y, x


def right(v):
    x, y = v
    return y, -x


def turn(v):
    x, y = v
    return -x, -y


def steps(l, confused=None):
    d = {'L': left, 'R': right, 'A': turn}
    if confused:
        d = {'L': right, 'R': left, 'A': turn}
    v = 1, 0
    for f in l:
        v = d[f](v)
        yield v


def lineUp(l):
    l1 = steps(l)
    l2 = steps(l, confused=True)
    return len(list(filter(lambda t: t[0] == t[1], zip(l1, l2))))


class MyTest(unittest.TestCase):

    def test01(self):
        l = 'LLARL'
        y = 3
        self.assertEqual(lineUp(l), y)

    def test02(self):
        l = 'RLR'
        y = 1
        self.assertEqual(lineUp(l), y)

    def test03(self):
        l = ''
        y = 0
        self.assertEqual(lineUp(l), y)

    def test04(self):
        l = 'L'
        y = 0
        self.assertEqual(lineUp(l), y)

    def test05(self):
        l = 'A'
        y = 1
        self.assertEqual(lineUp(l), y)

    def test06(self):
        l = 'AAAAAAAAAAAAAAA'
        y = 15
        self.assertEqual(lineUp(l), y)

    def test07(self):
        l = 'RRRRRRRRRRLLLLLLLLLRRRRLLLLLLLLLL'
        y = 16
        self.assertEqual(lineUp(l), y)

    def test08(self):
        l = 'AALAAALARAR'
        y = 5
        self.assertEqual(lineUp(l), y)


if __name__ == '__main__':
    unittest.main()
