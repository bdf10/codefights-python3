import unittest


def concatenateArrays(a, b):
    return a + b


class MyTest(unittest.TestCase):

    def test01(self):
        a, b = [2, 2, 1], [10, 11]
        y = [2, 2, 1, 10, 11]
        self.assertEqual(concatenateArrays(a, b), y)

    def test02(self):
        a, b = [1, 2], [3, 1, 2]
        y = [1, 2, 3, 1, 2]
        self.assertEqual(concatenateArrays(a, b), y)

    def test03(self):
        a, b = [1], []
        y = [1]
        self.assertEqual(concatenateArrays(a, b), y)

    def test04(self):
        a, b = [2, 10, 3, 9, 5, 11, 11], [4, 8, 1, 13, 3, 1, 14]
        y = [2, 10, 3, 9, 5, 11, 11, 4, 8, 1, 13, 3, 1, 14]
        self.assertEqual(concatenateArrays(a, b), y)

    def test05(self):
        a, b = [9, 6, 6, 9, 8, 14], [3, 2, 2, 5, 3, 11, 12, 9, 7, 7]
        y = [9, 6, 6, 9, 8, 14, 3, 2, 2, 5, 3, 11, 12, 9, 7, 7]
        self.assertEqual(concatenateArrays(a, b), y)


if __name__ == '__main__':
    unittest.main()
