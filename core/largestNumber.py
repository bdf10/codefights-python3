import unittest


def largestNumber(n):
    return int('9' * n)


class MyTest(unittest.TestCase):

    def test01(self):
        n = 2
        y = 99
        self.assertEqual(largestNumber(n), y)

    def test02(self):
        n = 1
        y = 9
        self.assertEqual(largestNumber(n), y)

    def test03(self):
        n = 7
        y = 9999999
        self.assertEqual(largestNumber(n), y)

    def test04(self):
        n = 4
        y = 9999
        self.assertEqual(largestNumber(n), y)

    def test05(self):
        n = 3
        y = 999
        self.assertEqual(largestNumber(n), y)


if __name__ == '__main__':
    unittest.main()
