import unittest


def countSumOfTwoRepresentations2(n, l, r):
    for k in range(l, r + 1):
        if l <= n - k <= r:
            return (len(range(k, n-k)) + 2) // 2
    return 0


class MyTest(unittest.TestCase):

    def test01(self):
        n, l, r = 6, 2, 4
        y = 2
        self.assertEqual(countSumOfTwoRepresentations2(n, l, r), y)

    def test02(self):
        n, l, r = 6, 3, 3
        y = 1
        self.assertEqual(countSumOfTwoRepresentations2(n, l, r), y)

    def test03(self):
        n, l, r = 10, 9, 11
        y = 0
        self.assertEqual(countSumOfTwoRepresentations2(n, l, r), y)

    def test04(self):
        n, l, r = 24, 8, 16
        y = 5
        self.assertEqual(countSumOfTwoRepresentations2(n, l, r), y)

    def test05(self):
        n, l, r = 24, 12, 12
        y = 1
        self.assertEqual(countSumOfTwoRepresentations2(n, l, r), y)

    def test06(self):
        n, l, r = 93, 24, 58
        y = 12
        self.assertEqual(countSumOfTwoRepresentations2(n, l, r), y)

    # def test07(self):
    #     n, l, r = 1000000, 490000, 900000
    #     y = 10001
    #     self.assertEqual(countSumOfTwoRepresentations2(n, l, r), y)


if __name__ == '__main__':
    unittest.main()
