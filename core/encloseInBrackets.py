import unittest


def encloseInBrackets(s):
    return '(' + s + ')'


class MyTest(unittest.TestCase):

    def test01(self):
        s = 'abacaba'
        y = '(abacaba)'
        self.assertEqual(encloseInBrackets(s), y)

    def test02(self):
        s = 'abcdef'
        y = '(abcdef)'
        self.assertEqual(encloseInBrackets(s), y)

    def test03(self):
        s = 'aaad'
        y = '(aaad)'
        self.assertEqual(encloseInBrackets(s), y)


if __name__ == '__main__':
    unittest.main()
