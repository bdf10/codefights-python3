from itertools import count
import unittest


def leastFactorial(n):
    f = 1
    for k in count(1):
        f *= k
        if f >= n:
            return f


class MyTest(unittest.TestCase):

    def test01(self):
        n = 17
        y = 24
        self.assertEqual(leastFactorial(n), y)

    def test02(self):
        n = 1
        y = 1
        self.assertEqual(leastFactorial(n), y)

    def test03(self):
        n = 5
        y = 6
        self.assertEqual(leastFactorial(n), y)


if __name__ == '__main__':
    unittest.main()
