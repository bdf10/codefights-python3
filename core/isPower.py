import unittest
from math import log


def isPower(n):
    if n == 1:
        return True
    for b in range(2, n + 1):
        l = int(log(n, b))
        if l < 2:
            return False
        elif b ** l == n:
            return True


class MyTest(unittest.TestCase):

    def test01(self):
        n = 125
        y = True
        self.assertEqual(isPower(n), y)

    def test02(self):
        n = 72
        y = False
        self.assertEqual(isPower(n), y)

    def test03(self):
        n = 100
        y = True
        self.assertEqual(isPower(n), y)

    def test04(self):
        n = 11
        y = False
        self.assertEqual(isPower(n), y)

    def test05(self):
        n = 324
        y = True
        self.assertEqual(isPower(n), y)

    def test06(self):
        n = 256
        y = True
        self.assertEqual(isPower(n), y)

    def test07(self):
        n = 119
        y = False
        self.assertEqual(isPower(n), y)

    def test08(self):
        n = 400
        y = True
        self.assertEqual(isPower(n), y)

    def test09(self):
        n = 350
        y = False
        self.assertEqual(isPower(n), y)

    def test10(self):
        n = 361
        y = True
        self.assertEqual(isPower(n), y)


if __name__ == '__main__':
    unittest.main()
