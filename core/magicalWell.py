import unittest


def magicalWell(a, b, n):
    money = 0
    for _ in range(n):
        money += a * b
        a += 1
        b += 1
    return money


class MyTest(unittest.TestCase):

    def test01(self):
        a, b, n = 1, 2, 2
        y = 8
        self.assertEqual(magicalWell(a, b, n), y)

    def test02(self):
        a, b, n = 1, 1, 1
        y = 1
        self.assertEqual(magicalWell(a, b, n), y)

    def test03(self):
        a, b, n = 6, 5, 3
        y = 128
        self.assertEqual(magicalWell(a, b, n), y)


if __name__ == '__main__':
    unittest.main()
