import unittest
from itertools import count


def pagesNumberingWithInk(current, n):
    g = count(current)
    current = next(g)
    printed = len(str(current))
    while printed <= n:
        current = next(g)
        printed += len(str(current))
    return current - 1


class MyTest(unittest.TestCase):

    def test01(self):
        current, n = 1, 5
        y = 5
        self.assertEqual(pagesNumberingWithInk(current, n), y)

    def test02(self):
        current, n = 21, 5
        y = 22
        self.assertEqual(pagesNumberingWithInk(current, n), y)

    def test03(self):
        current, n = 8, 4
        y = 10
        self.assertEqual(pagesNumberingWithInk(current, n), y)

    def test04(self):
        current, n = 21, 6
        y = 23
        self.assertEqual(pagesNumberingWithInk(current, n), y)

    def test05(self):
        current, n = 76, 250
        y = 166
        self.assertEqual(pagesNumberingWithInk(current, n), y)

    def test06(self):
        current, n = 80, 1000
        y = 419
        self.assertEqual(pagesNumberingWithInk(current, n), y)


if __name__ == '__main__':
    unittest.main()
