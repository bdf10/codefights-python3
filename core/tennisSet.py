import unittest


def tennisSet(score1, score2):
    m, M = sorted([score1, score2])
    return (m < 5 and M == 6) or (5 <= m < M and M == 7)


class MyTest(unittest.TestCase):

    def test01(self):
        score1, score2 = 3, 6
        y = True
        self.assertEqual(tennisSet(score1, score2), y)

    def test02(self):
        score1, score2 = 8, 5
        y = False
        self.assertEqual(tennisSet(score1, score2), y)

    def test03(self):
        score1, score2 = 6, 5
        y = False
        self.assertEqual(tennisSet(score1, score2), y)

    def test04(self):
        score1, score2 = 7, 7
        y = False
        self.assertEqual(tennisSet(score1, score2), y)

    def test05(self):
        score1, score2 = 6, 4
        y = True
        self.assertEqual(tennisSet(score1, score2), y)

    def test06(self):
        score1, score2 = 7, 5
        y = True
        self.assertEqual(tennisSet(score1, score2), y)

    def test07(self):
        score1, score2 = 7, 2
        y = False
        self.assertEqual(tennisSet(score1, score2), y)

    def test08(self):
        score1, score2 = 7, 6
        y = True
        self.assertEqual(tennisSet(score1, score2), y)


if __name__ == '__main__':
    unittest.main()
