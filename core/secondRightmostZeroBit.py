import unittest


def secondRightmostZeroBit(n):
    s = ''.join(reversed('{:b}'.format(n)))
    return 2 ** int(s.index('0', s.index('0') + 1))


class MyTest(unittest.TestCase):

    def test01(self):
        n = 37
        y = 8
        self.assertEqual(secondRightmostZeroBit(n), y)

    def test02(self):
        n = 1073741824
        y = 2
        self.assertEqual(secondRightmostZeroBit(n), y)

    def test03(self):
        n = 83748
        y = 2
        self.assertEqual(secondRightmostZeroBit(n), y)

    def test04(self):
        n = 4
        y = 2
        self.assertEqual(secondRightmostZeroBit(n), y)

    def test05(self):
        n = 728782938
        y = 4
        self.assertEqual(secondRightmostZeroBit(n), y)


if __name__ == '__main__':
    unittest.main()
