import unittest


def reachNextLevel(experience, threshold, reward):
    return experience + reward >= threshold


class MyTest(unittest.TestCase):

    def test01(self):
        experience, threshold, reward = 10, 15, 5
        y = True
        self.assertEqual(reachNextLevel(experience, threshold, reward), y)

    def test02(self):
        experience, threshold, reward = 10, 15, 4
        y = False
        self.assertEqual(reachNextLevel(experience, threshold, reward), y)

    def test03(self):
        experience, threshold, reward = 3, 6, 4
        y = True
        self.assertEqual(reachNextLevel(experience, threshold, reward), y)


if __name__ == '__main__':
    unittest.main()
