from string import ascii_lowercase, hexdigits
import unittest


def isMAC48Address(st):
    hex = set(hexdigits) - set(ascii_lowercase)
    split = st.split('-')
    if len(split) != 6:
        return False
    for c in split:
        if len(c) != 2:
            return False
        if not set(c).issubset(hex):
            return False
    return True


class MyTest(unittest.TestCase):

    def test01(self):
        st = '00-1B-63-84-45-E6'
        y = True
        self.assertEqual(isMAC48Address(st), y)

    def test02(self):
        st = 'Z1-1B-63-84-45-E6'
        y = False
        self.assertEqual(isMAC48Address(st), y)

    def test03(self):
        st = 'not a MAC-48 address'
        y = False
        self.assertEqual(isMAC48Address(st), y)

    def test04(self):
        st = 'FF-FF-FF-FF-FF-FF'
        y = True
        self.assertEqual(isMAC48Address(st), y)

    def test05(self):
        st = '00-00-00-00-00-00'
        y = True
        self.assertEqual(isMAC48Address(st), y)

    def test06(self):
        st = 'G0-00-00-00-00-00'
        y = False
        self.assertEqual(isMAC48Address(st), y)

    def test07(self):
        st = '02-03-04-05-06-07-'
        y = False
        self.assertEqual(isMAC48Address(st), y)

    def test08(self):
        st = '12-34-56-78-9A-BC'
        y = True
        self.assertEqual(isMAC48Address(st), y)

    def test09(self):
        st = 'FF-FF-AB-CD-EA-BC'
        y = True
        self.assertEqual(isMAC48Address(st), y)

    def test10(self):
        st = '12-34-56-78-9A-BG'
        y = False
        self.assertEqual(isMAC48Address(st), y)


if __name__ == '__main__':
    unittest.main()
