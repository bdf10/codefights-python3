import unittest


def yieldround(n):
    bump = 0
    for digit in map(int, str(n)[1:][::-1]):
        digit += bump
        yield '0'
        bump = int(digit >= 5)
    yield str(int(str(n)[0]) + bump)[::-1]


def rounders(n):
    return int(''.join(yieldround(n))[::-1])


class MyTest(unittest.TestCase):

    def test01(self):
        n = 15
        y = 20
        self.assertEqual(rounders(n), y)

    def test02(self):
        n = 1234
        y = 1000
        self.assertEqual(rounders(n), y)

    def test03(self):
        n = 1445
        y = 2000
        self.assertEqual(rounders(n), y)

    def test04(self):
        n = 14
        y = 10
        self.assertEqual(rounders(n), y)

    def test05(self):
        n = 10
        y = 10
        self.assertEqual(rounders(n), y)

    def test06(self):
        n = 99
        y = 100
        self.assertEqual(rounders(n), y)


if __name__ == '__main__':
    unittest.main()
