import unittest


def arrayPacking(a):
    b = ''.join(reversed([format(n, '08b') for n in a]))
    return int(b, 2)


class MyTest(unittest.TestCase):

    def test01(self):
        a = [24, 85, 0]
        y = 21784
        self.assertEqual(arrayPacking(a), y)

    def test02(self):
        a = [23, 45, 39]
        y = 2567447
        self.assertEqual(arrayPacking(a), y)

    def test03(self):
        a = [1, 2, 4, 8]
        y = 134480385
        self.assertEqual(arrayPacking(a), y)

    def test04(self):
        a = [5]
        y = 5
        self.assertEqual(arrayPacking(a), y)

    def test05(self):
        a = [187, 99, 42, 43]
        y = 724198331
        self.assertEqual(arrayPacking(a), y)


if __name__ == '__main__':
    unittest.main()
