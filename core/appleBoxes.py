import unittest


def evensum(k):
    n = k // 2
    return 2 * n * (n + 1) * (2 * n + 1) // 3

def oddsum(k):
    n = (k - 1) // 2
    return  (n + 1) * (2 * n + 3) * (2 * n + 1) // 3


def appleBoxes(k):
    return evensum(k) - oddsum(k)


class MyTest(unittest.TestCase):

    def test01(self):
        k = 5
        y = -15
        self.assertEqual(appleBoxes(k), y)

    def test02(self):
        k = 15
        y = -120
        self.assertEqual(appleBoxes(k), y)

    def test03(self):
        k = 36
        y = 666
        self.assertEqual(appleBoxes(k), y)

    def test04(self):
        k = 1
        y = -1
        self.assertEqual(appleBoxes(k), y)


if __name__ == '__main__':
    unittest.main()
