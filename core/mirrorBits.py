import unittest


def mirrorBits(a):
    b = ''.join(reversed('{:b}'.format(a)))
    return int(b, 2)


class MyTest(unittest.TestCase):

    def test01(self):
        a = 97
        y = 67
        self.assertEqual(mirrorBits(a), y)

    def test02(self):
        a = 8
        y = 1
        self.assertEqual(mirrorBits(a), y)

    def test03(self):
        a = 123
        y = 111
        self.assertEqual(mirrorBits(a), y)

    def test04(self):
        a = 86782
        y = 65173
        self.assertEqual(mirrorBits(a), y)

    def test05(self):
        a = 5
        y = 5
        self.assertEqual(mirrorBits(a), y)


if __name__ == '__main__':
    unittest.main()
