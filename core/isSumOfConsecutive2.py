import unittest
from itertools import count


def isSumOfConsecutive2(n):
    g = count(0)
    for k in range(1, n):
        for j in range(2, n):
            s = j * (k + (j - 1) / 2)
            if s == n:
                next(g)
                break
            elif s > n:
                break
    return next(g)


class MyTest(unittest.TestCase):

    def test01(self):
        n = 9
        y = 2
        self.assertEqual(isSumOfConsecutive2(n), y)

    def test02(self):
        n = 8
        y = 0
        self.assertEqual(isSumOfConsecutive2(n), y)

    def test03(self):
        n = 15
        y = 3
        self.assertEqual(isSumOfConsecutive2(n), y)


if __name__ == '__main__':
    unittest.main()
