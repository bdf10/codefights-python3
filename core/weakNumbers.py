import unittest
from itertools import count


def ndivisors(n):
    g = count(1)
    for d in range(1, n // 2 + 1):
        if n % d == 0:
            next(g)
    return next(g)


def weakness(n, divsdict):
    g = count(0)
    divsn = divsdict[n]
    for key, value in divsdict.items():
        if key < n and value > divsn:
            next(g)
    return next(g)


def weakNumbers(n):
    divs = {k: ndivisors(k) for k in range(1, n + 1)}
    weaknesses = {k: weakness(k, divs) for k in range(1, n + 1)}
    m = max(weaknesses.values())
    return [m, len([key for key in weaknesses if weaknesses[key] == m])]


class MyTest(unittest.TestCase):

    def test01(self):
        n = 9
        y = [2, 2]
        self.assertEqual(weakNumbers(n), y)

    def test02(self):
        n = 1
        y = [0, 1]
        self.assertEqual(weakNumbers(n), y)

    def test03(self):
        n = 2
        y = [0, 2]
        self.assertEqual(weakNumbers(n), y)

    def test04(self):
        n = 7
        y = [2, 1]
        self.assertEqual(weakNumbers(n), y)

    def test05(self):
        n = 500
        y = [403, 1]
        self.assertEqual(weakNumbers(n), y)

    def test06(self):
        n = 4
        y = [0, 4]
        self.assertEqual(weakNumbers(n), y)


if __name__ == '__main__':
    unittest.main()
