import unittest


def arrayReplace(l, r, s):
    return list(map(lambda x: x if x != r else s, l))


class MyTest(unittest.TestCase):

    def test01(self):
        l, r, s = [1, 2, 1], 1, 3
        y = [3, 2, 3]
        self.assertEqual(arrayReplace(l, r, s), y)

    def test02(self):
        l, r, s = [1, 2, 3, 4, 5], 3, 0
        y = [1, 2, 0, 4, 5]
        self.assertEqual(arrayReplace(l, r, s), y)

    def test03(self):
        l, r, s = [1, 1, 1], 1, 10
        y = [10, 10, 10]
        self.assertEqual(arrayReplace(l, r, s), y)


if __name__ == '__main__':
    unittest.main()
