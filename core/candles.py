import unittest


def candles(candlesNumber, makeNew):
    burned = leftover = candlesNumber
    while leftover >= makeNew:
        burned += leftover // makeNew
        leftover = leftover // makeNew + leftover % makeNew
    return burned


class MyTest(unittest.TestCase):

    def test01(self):
        candlesNumber, makeNew = 5, 2
        y = 9
        self.assertEqual(candles(candlesNumber, makeNew), y)

    def test02(self):
        candlesNumber, makeNew = 1, 2
        y = 1
        self.assertEqual(candles(candlesNumber, makeNew), y)

    def test03(self):
        candlesNumber, makeNew = 3, 3
        y = 4
        self.assertEqual(candles(candlesNumber, makeNew), y)

    def test04(self):
        candlesNumber, makeNew = 11, 3
        y = 16
        self.assertEqual(candles(candlesNumber, makeNew), y)


if __name__ == '__main__':
    unittest.main()
