import unittest


def removeArrayPart(a, l, r):
    return [x for i, x in enumerate(a) if not l <= i <= r]


class MyTest(unittest.TestCase):

    def test01(self):
        a, l, r = [2, 3, 2, 3, 4, 5], 2, 4
        y = [2, 3, 5]
        self.assertEqual(removeArrayPart(a, l, r), y)

    def test02(self):
        a, l, r = [2, 4, 10, 1], 0, 2
        y = [1]
        self.assertEqual(removeArrayPart(a, l, r), y)

    def test03(self):
        a, l, r = [5, 3, 2, 3, 4], 1, 1
        y = [5, 2, 3, 4]
        self.assertEqual(removeArrayPart(a, l, r), y)


if __name__ == '__main__':
    unittest.main()
