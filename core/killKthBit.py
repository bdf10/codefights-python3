import unittest


def killKthBit(n, k):
    b = format(n, '031b')
    l = list(b)
    l[-k] = '0'
    return int(''.join(l), 2)


class MyTest(unittest.TestCase):

    def test01(self):
        n, k = 37, 3
        y = 33
        self.assertEqual(killKthBit(n, k), y)

    def test02(self):
        n, k = 37, 4
        y = 37
        self.assertEqual(killKthBit(n, k), y)

    def test03(self):
        n, k = 37, 2
        y = 37
        self.assertEqual(killKthBit(n, k), y)

    def test04(self):
        n, k = 0, 4
        y = 0
        self.assertEqual(killKthBit(n, k), y)

    def test05(self):
        n, k = 2147483647, 16
        y = 2147450879
        self.assertEqual(killKthBit(n, k), y)

    def test06(self):
        n, k = 374823748, 13
        y = 374819652
        self.assertEqual(killKthBit(n, k), y)

    def test07(self):
        n, k = 2734827, 4
        y = 2734819
        self.assertEqual(killKthBit(n, k), y)

    def test08(self):
        n, k = 1084197039, 15
        y = 1084197039
        self.assertEqual(killKthBit(n, k), y)

    def test09(self):
        n, k = 1160825071, 3
        y = 1160825067
        self.assertEqual(killKthBit(n, k), y)

    def test10(self):
        n, k = 2039063284, 4
        y = 2039063284
        self.assertEqual(killKthBit(n, k), y)


if __name__ == '__main__':
    unittest.main()
