import unittest


def isTandemRepeat(s):
    n = len(s) // 2
    return s[n:] == s[:n]


class MyTest(unittest.TestCase):

    def test01(self):
        s = 'tandemtandem'
        y = True
        self.assertEqual(isTandemRepeat(s), y)

    def test02(self):
        s = 'qqq'
        y = False
        self.assertEqual(isTandemRepeat(s), y)

    def test03(self):
        s = '2w2ww'
        y = False
        self.assertEqual(isTandemRepeat(s), y)

    def test04(self):
        s = 'hophey'
        y = False
        self.assertEqual(isTandemRepeat(s), y)

    def test05(self):
        s = 'CodeFightsCodeFights'
        y = True
        self.assertEqual(isTandemRepeat(s), y)

    def test06(self):
        s = 'interestinterest'
        y = True
        self.assertEqual(isTandemRepeat(s), y)

    def test07(self):
        s = 'aa'
        y = True
        self.assertEqual(isTandemRepeat(s), y)

    def test08(self):
        s = 'ab'
        y = False
        self.assertEqual(isTandemRepeat(s), y)

    def test09(self):
        s = 'stringString'
        y = False
        self.assertEqual(isTandemRepeat(s), y)

    def test10(self):
        s = 'truetruetrue'
        y = False
        self.assertEqual(isTandemRepeat(s), y)


if __name__ == '__main__':
    unittest.main()
