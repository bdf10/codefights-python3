import unittest


def isInfiniteProcess(a, b):
    k = b - a
    return k < 0 or bool(k % 2)


class MyTest(unittest.TestCase):

    def test01(self):
        a, b = 2, 6
        y = False
        self.assertEqual(isInfiniteProcess(a, b), y)

    def test02(self):
        a, b = 2, 3
        y = True
        self.assertEqual(isInfiniteProcess(a, b), y)

    def test03(self):
        a, b = 2, 10
        y = False
        self.assertEqual(isInfiniteProcess(a, b), y)

    def test04(self):
        a, b = 0, 1
        y = True
        self.assertEqual(isInfiniteProcess(a, b), y)

    def test05(self):
        a, b = 3, 1
        y = True
        self.assertEqual(isInfiniteProcess(a, b), y)

    def test06(self):
        a, b = 10, 10
        y = False
        self.assertEqual(isInfiniteProcess(a, b), y)

    def test07(self):
        a, b = 5, 10
        y = True
        self.assertEqual(isInfiniteProcess(a, b), y)

    def test08(self):
        a, b = 6, 10
        y = False
        self.assertEqual(isInfiniteProcess(a, b), y)

    def test09(self):
        a, b = 10, 0
        y = True
        self.assertEqual(isInfiniteProcess(a, b), y)

    def test10(self):
        a, b = 5, 5
        y = False
        self.assertEqual(isInfiniteProcess(a, b), y)


if __name__ == '__main__':
    unittest.main()
