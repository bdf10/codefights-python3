import unittest


def sumsquares(n):
    return sum(int(x)**2 for x in str(n))


def squareDigitsSequence(a0):
    s = {a0}
    x = sumsquares(a0)
    while not x in s:
        s.add(x)
        x = sumsquares(x)
    return len(s) + 1


class MyTest(unittest.TestCase):

    def test01(self):
        a0 = 16
        y = 9
        self.assertEqual(squareDigitsSequence(a0), y)

    def test02(self):
        a0 = 103
        y = 4
        self.assertEqual(squareDigitsSequence(a0), y)

    def test03(self):
        a0 = 1
        y = 2
        self.assertEqual(squareDigitsSequence(a0), y)


if __name__ == '__main__':
    unittest.main()
