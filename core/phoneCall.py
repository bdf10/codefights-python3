from itertools import count
import unittest


def phoneCall(min1, min2_10, min11, s):
    def cost(m):
        c = int(m > 0) * min1
        c += sum(int(x <= m) for x in range(2, 11)) * min2_10
        c += int(m - 10 > 0) * (m - 10) * min11
        return c
    for m in count(0):
        if cost(m) > s:
            return m - 1


class MyTest(unittest.TestCase):

    def test01(self):
        min1, min2_10, min11, s = 3, 1, 2, 20
        y = 14
        self.assertEqual(phoneCall(min1, min2_10, min11, s), y)

    def test02(self):
        min1, min2_10, min11, s = 2, 2, 1, 2
        y = 1
        self.assertEqual(phoneCall(min1, min2_10, min11, s), y)

    def test03(self):
        min1, min2_10, min11, s = 10, 1, 2, 22
        y = 11
        self.assertEqual(phoneCall(min1, min2_10, min11, s), y)

    def test04(self):
        min1, min2_10, min11, s = 2, 2, 1, 24
        y = 14
        self.assertEqual(phoneCall(min1, min2_10, min11, s), y)

    def test05(self):
        min1, min2_10, min11, s = 1, 2, 1, 6
        y = 3
        self.assertEqual(phoneCall(min1, min2_10, min11, s), y)


if __name__ == '__main__':
    unittest.main()
