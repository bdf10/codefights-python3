import unittest


def increaseNumberRoundness(n):
    digits = str(n)
    if not '0' in digits:
        return False
    a = digits.index('0')
    b = len(digits) - next((i for i, x in enumerate(reversed(digits)) if x != '0'), False)
    return a < b



class MyTest(unittest.TestCase):

    def test01(self):
        n = 902200100
        y = True
        self.assertEqual(increaseNumberRoundness(n), y)

    def test02(self):
        n = 11000
        y = False
        self.assertEqual(increaseNumberRoundness(n), y)

    def test03(self):
        n = 99080
        y = True
        self.assertEqual(increaseNumberRoundness(n), y)

    def test04(self):
        n = 1022220
        y = True
        self.assertEqual(increaseNumberRoundness(n), y)

    def test05(self):
        n = 106611
        y = True
        self.assertEqual(increaseNumberRoundness(n), y)

    def test06(self):
        n = 234230
        y = False
        self.assertEqual(increaseNumberRoundness(n), y)

    def test07(self):
        n = 888
        y = False
        self.assertEqual(increaseNumberRoundness(n), y)

    def test08(self):
        n = 100
        y = False
        self.assertEqual(increaseNumberRoundness(n), y)

    def test09(self):
        n = 1000000000
        y = False
        self.assertEqual(increaseNumberRoundness(n), y)

    def test10(self):
        n = 103456789
        y = True
        self.assertEqual(increaseNumberRoundness(n), y)


if __name__ == '__main__':
    unittest.main()
