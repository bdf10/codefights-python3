import unittest


def rangeBitCount(a, b):
    s = ''.join('{:b}'.format(x) for x in range(a, b + 1))
    return sum(int(x) for x in s)


class MyTest(unittest.TestCase):

    def test01(self):
        a, b = 2, 7
        y = 11
        self.assertEqual(rangeBitCount(a, b), y)

    def test02(self):
        a, b = 0, 1
        y = 1
        self.assertEqual(rangeBitCount(a, b), y)

    def test03(self):
        a, b = 1, 10
        y = 17
        self.assertEqual(rangeBitCount(a, b), y)

    def test04(self):
        a, b = 8, 9
        y = 3
        self.assertEqual(rangeBitCount(a, b), y)

    def test05(self):
        a, b = 9, 10
        y = 4
        self.assertEqual(rangeBitCount(a, b), y)


if __name__ == '__main__':
    unittest.main()
