import unittest


def maxMultiple(divisor, bound):
    return bound - (bound % divisor)


class MyTest(unittest.TestCase):

    def test01(self):
        divisor, bound = 3, 10
        y = 9
        self.assertEqual(maxMultiple(divisor, bound), y)

    def test02(self):
        divisor, bound = 2, 7
        y = 6
        self.assertEqual(maxMultiple(divisor, bound), y)

    def test03(self):
        divisor, bound = 10, 50
        y = 50
        self.assertEqual(maxMultiple(divisor, bound), y)

    def test04(self):
        divisor, bound = 7, 100
        y = 98
        self.assertEqual(maxMultiple(divisor, bound), y)


if __name__ == '__main__':
    unittest.main()
