import unittest


def extraNumber(a, b, c):
    t = (a, b, c)
    for x in t:
        if t.count(x) == 1:
            return x


class MyTest(unittest.TestCase):

    def test01(self):
        a, b, c = 2, 7, 2
        y = 7
        self.assertEqual(extraNumber(a, b, c), y)

    def test02(self):
        a, b, c = 3, 2, 2
        y = 3
        self.assertEqual(extraNumber(a, b, c), y)

    def test03(self):
        a, b, c = 5, 5, 1
        y = 1
        self.assertEqual(extraNumber(a, b, c), y)

    def test04(self):
        a, b, c = 500000000, 3, 500000000
        y = 3
        self.assertEqual(extraNumber(a, b, c), y)


if __name__ == '__main__':
    unittest.main()
