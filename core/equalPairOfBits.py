import unittest


def equalPairOfBits(n, m):
    bn = reversed(format(n, '032b'))
    bm = reversed(format(m, '032b'))
    for i, x, y in zip(range(32), bn, bm):
        if x == y:
            return 2 ** i


class MyTest(unittest.TestCase):

    def test01(self):
        n, m = 10, 11
        y = 2
        self.assertEqual(equalPairOfBits(n, m), y)

    def test02(self):
        n, m = 0, 0
        y = 1
        self.assertEqual(equalPairOfBits(n, m), y)

    def test03(self):
        n, m = 28, 27
        y = 8
        self.assertEqual(equalPairOfBits(n, m), y)

    def test04(self):
        n, m = 895, 928
        y = 32
        self.assertEqual(equalPairOfBits(n, m), y)

    def test05(self):
        n, m = 1073741824, 1006895103
        y = 262144
        self.assertEqual(equalPairOfBits(n, m), y)


if __name__ == '__main__':
    unittest.main()
