from collections import namedtuple
import unittest


def knapsackLight(value1, weight1, value2, weight2, maxW):
    Item = namedtuple('Item', ['value', 'weight'])
    i1 = Item(value1, weight1)
    i2 = Item(value2, weight2)
    i1, i2 = sorted([i1, i2], key=lambda t: t.value)
    if maxW >= i1.weight + i2.weight:
        return i1.value + i2.value
    if maxW >= i2.weight:
        return i2.value
    if maxW >= i1.weight:
        return i1.value
    return 0


class MyTest(unittest.TestCase):

    def test01(self):
        value1, weight1, value2, weight2, maxW = 10, 5, 6, 4, 8
        y = 10
        self.assertEqual(knapsackLight(
            value1, weight1, value2, weight2, maxW), y)

    def test02(self):
        value1, weight1, value2, weight2, maxW = 10, 5, 6, 4, 9
        y = 16
        self.assertEqual(knapsackLight(
            value1, weight1, value2, weight2, maxW), y)

    def test03(self):
        value1, weight1, value2, weight2, maxW = 5, 3, 7, 4, 6
        y = 7
        self.assertEqual(knapsackLight(
            value1, weight1, value2, weight2, maxW), y)

    def test04(self):
        value1, weight1, value2, weight2, maxW = 10, 2, 11, 3, 1
        y = 0
        self.assertEqual(knapsackLight(
            value1, weight1, value2, weight2, maxW), y)

    def test05(self):
        value1, weight1, value2, weight2, maxW = 15, 2, 20, 3, 2
        y = 15
        self.assertEqual(knapsackLight(
            value1, weight1, value2, weight2, maxW), y)

    def test06(self):
        value1, weight1, value2, weight2, maxW = 2, 5, 3, 4, 5
        y = 3
        self.assertEqual(knapsackLight(
            value1, weight1, value2, weight2, maxW), y)

    def test07(self):
        value1, weight1, value2, weight2, maxW = 4, 3, 3, 4, 4
        y = 4
        self.assertEqual(knapsackLight(
            value1, weight1, value2, weight2, maxW), y)

    def test08(self):
        value1, weight1, value2, weight2, maxW = 3, 5, 3, 8, 10
        y = 3
        self.assertEqual(knapsackLight(
            value1, weight1, value2, weight2, maxW), y)


if __name__ == '__main__':
    unittest.main()
