import unittest


def seatsInTheater(nCols, nRows, col, row):
    return (nRows - row) + (nCols - col) * (nRows - row)


class MyTest(unittest.TestCase):

    def test01(self):
        nCols, nRows, col, row = 16, 11, 5, 3
        y = 96
        self.assertEqual(seatsInTheater(nCols, nRows, col, row), y)

    def test02(self):
        nCols, nRows, col, row = 1, 1, 1, 1
        y = 0
        self.assertEqual(seatsInTheater(nCols, nRows, col, row), y)

    def test03(self):
        nCols, nRows, col, row = 13, 6, 8, 3
        y = 18
        self.assertEqual(seatsInTheater(nCols, nRows, col, row), y)

    def test04(self):
        nCols, nRows, col, row = 60, 100, 60, 1
        y = 99
        self.assertEqual(seatsInTheater(nCols, nRows, col, row), y)

    def test05(self):
        nCols, nRows, col, row = 1000, 1000, 1000, 1000
        y = 0
        self.assertEqual(seatsInTheater(nCols, nRows, col, row), y)


if __name__ == '__main__':
    unittest.main()
