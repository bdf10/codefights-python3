import unittest


def circleOfNumbers(n, firstNumber):
    return (firstNumber + n // 2) % n


class MyTest(unittest.TestCase):

    def test01(self):
        n, firstNumber = 10, 2
        y = 7
        self.assertEqual(circleOfNumbers(n, firstNumber), y)

    def test02(self):
        n, firstNumber = 10, 7
        y = 2
        self.assertEqual(circleOfNumbers(n, firstNumber), y)

    def test03(self):
        n, firstNumber = 4, 1
        y = 3
        self.assertEqual(circleOfNumbers(n, firstNumber), y)

    def test04(self):
        n, firstNumber = 6, 3
        y = 0
        self.assertEqual(circleOfNumbers(n, firstNumber), y)


if __name__ == '__main__':
    unittest.main()
