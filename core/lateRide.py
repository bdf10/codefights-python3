import unittest


def lateRide(n):
    hrs = str(n // 60)
    mins = str(n % 60)
    return sum(int(digit) for digit in hrs + mins)


class MyTest(unittest.TestCase):

    def test01(self):
        n = 240
        y = 4
        self.assertEqual(lateRide(n), y)

    def test02(self):
        n = 808
        y = 14
        self.assertEqual(lateRide(n), y)

    def test03(self):
        n = 1439
        y = 19
        self.assertEqual(lateRide(n), y)

    def test04(self):
        n = 0
        y = 0
        self.assertEqual(lateRide(n), y)

    def test05(self):
        n = 23
        y = 5
        self.assertEqual(lateRide(n), y)

    def test06(self):
        n = 8
        y = 8
        self.assertEqual(lateRide(n), y)


if __name__ == '__main__':
    unittest.main()
