import unittest


def addTwoDigits(n):
    return sum(int(digit) for digit in str(n))


class MyTest(unittest.TestCase):

    def test01(self):
        n = 29
        y = 11
        self.assertEqual(addTwoDigits(n), y)

    def test02(self):
        n = 48
        y = 12
        self.assertEqual(addTwoDigits(n), y)

    def test03(self):
        n = 10
        y = 1
        self.assertEqual(addTwoDigits(n), y)

    def test04(self):
        n = 25
        y = 7
        self.assertEqual(addTwoDigits(n), y)


if __name__ == '__main__':
    unittest.main()
