import unittest


def replaceMiddle(a):
    n = len(a)
    m = n // 2
    if n % 2 == 0:
        middle = a[m] + a[m - 1]
        a[m] = middle
        del a[m - 1]
    return a


class MyTest(unittest.TestCase):

    def test01(self):
        a = [7, 2, 2, 5, 10, 7]
        y = [7, 2, 7, 10, 7]
        self.assertEqual(replaceMiddle(a), y)

    def test02(self):
        a = [-5, -5, 10]
        y = [-5, -5, 10]
        self.assertEqual(replaceMiddle(a), y)

    def test03(self):
        a = [45, 23, 12, 33, 12, 453, -234, -45]
        y = [45, 23, 12, 45, 453, -234, -45]
        self.assertEqual(replaceMiddle(a), y)

    def test04(self):
        a = [2, 8]
        y = [10]
        self.assertEqual(replaceMiddle(a), y)

    def test05(self):
        a = [-12, 34, 40, -5, -12, 4, 0, 0, -12]
        y = [-12, 34, 40, -5, -12, 4, 0, 0, -12]
        self.assertEqual(replaceMiddle(a), y)

    def test06(self):
        a = [9, 0, 15, 9]
        y = [9, 15, 9]
        self.assertEqual(replaceMiddle(a), y)

    def test07(self):
        a = [-6, 6, -6]
        y = [-6, 6, -6]
        self.assertEqual(replaceMiddle(a), y)

    def test08(self):
        a = [26, 26, -17]
        y = [26, 26, -17]
        self.assertEqual(replaceMiddle(a), y)

    def test09(self):
        a = [-7, 5, 5, 10]
        y = [-7, 10, 10]
        self.assertEqual(replaceMiddle(a), y)


if __name__ == '__main__':
    unittest.main()
