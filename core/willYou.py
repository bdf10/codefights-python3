import unittest


def willYou(young, beautiful, loved):
    return loved != (young and beautiful)


class MyTest(unittest.TestCase):

    def test01(self):
        young, beautiful, loved = True, True, True
        y = False
        self.assertEqual(willYou(young, beautiful, loved), y)

    def test02(self):
        young, beautiful, loved = True, False, True
        y = True
        self.assertEqual(willYou(young, beautiful, loved), y)

    def test03(self):
        young, beautiful, loved = False, False, False
        y = False
        self.assertEqual(willYou(young, beautiful, loved), y)

    def test04(self):
        young, beautiful, loved = False, False, True
        y = True
        self.assertEqual(willYou(young, beautiful, loved), y)


if __name__ == '__main__':
    unittest.main()
