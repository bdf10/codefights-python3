import unittest
from itertools import count


def s(x):
    return sum(int(digit) for digit in str(x))


def comfortableNumbers(l, r):
    g = count(0)
    for a in range(l, r):
        for b in range(a + 1, min([r, a + s(a)]) + 1):
            if b - s(b) <= a:
                next(g)
    return next(g)


class MyTest(unittest.TestCase):

    def test01(self):
        l, r = 10, 12
        y = 2
        self.assertEqual(comfortableNumbers(l, r), y)

    def test02(self):
        l, r = 1, 9
        y = 20
        self.assertEqual(comfortableNumbers(l, r), y)

    def test03(self):
        l, r = 13, 13
        y = 0
        self.assertEqual(comfortableNumbers(l, r), y)

    def test04(self):
        l, r = 12, 108
        y = 707
        self.assertEqual(comfortableNumbers(l, r), y)

    def test05(self):
        l, r = 239, 777
        y = 6166
        self.assertEqual(comfortableNumbers(l, r), y)

    def test06(self):
        l, r = 1, 1000
        y = 11435
        self.assertEqual(comfortableNumbers(l, r), y)


if __name__ == '__main__':
    unittest.main()
