import unittest


def isCaseInsensitivePalindrome(s):
    s = s.lower()
    return s[::-1] == s


class MyTest(unittest.TestCase):

    def test01(self):
        s = 'AaBaa'
        y = True
        self.assertEqual(isCaseInsensitivePalindrome(s), y)

    def test02(self):
        s = 'abac'
        y = False
        self.assertEqual(isCaseInsensitivePalindrome(s), y)

    def test03(self):
        s = 'aBACaba'
        y = True
        self.assertEqual(isCaseInsensitivePalindrome(s), y)

    def test04(self):
        s = 'opOP'
        y = False
        self.assertEqual(isCaseInsensitivePalindrome(s), y)

    def test05(self):
        s = 'ZZzzazZzz'
        y = True
        self.assertEqual(isCaseInsensitivePalindrome(s), y)

    def test06(self):
        s = 'zzzzazzzz'
        y = True
        self.assertEqual(isCaseInsensitivePalindrome(s), y)

    def test07(self):
        s = 'ZZzzabzZzz'
        y = False
        self.assertEqual(isCaseInsensitivePalindrome(s), y)

    def test08(self):
        s = 'abcdcbA'
        y = True
        self.assertEqual(isCaseInsensitivePalindrome(s), y)

    def test09(self):
        s = 'abracabra'
        y = False
        self.assertEqual(isCaseInsensitivePalindrome(s), y)

    def test10(self):
        s = 'abcd'
        y = False
        self.assertEqual(isCaseInsensitivePalindrome(s), y)


if __name__ == '__main__':
    unittest.main()
