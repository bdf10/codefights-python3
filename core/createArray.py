import unittest


def createArray(n):
    return [1] * n


class MyTest(unittest.TestCase):

    def test01(self):
        n = 4
        y = [1, 1, 1, 1]
        self.assertEqual(createArray(n), y)

    def test02(self):
        n = 2
        y = [1, 1]
        self.assertEqual(createArray(n), y)

    def test03(self):
        n = 1
        y = [1]
        self.assertEqual(createArray(n), y)

    def test04(self):
        n = 5
        y = [1, 1, 1, 1, 1]
        self.assertEqual(createArray(n), y)

    def test05(self):
        n = 49
        y = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
             1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        self.assertEqual(createArray(n), y)


if __name__ == '__main__':
    unittest.main()
