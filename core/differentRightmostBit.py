import unittest


def differentRightmostBit(n, m):
    bn = reversed(format(n, '032b'))
    bm = reversed(format(m, '032b'))
    for i, x, y in zip(range(32), bn, bm):
        if x != y:
            return 2 ** i


class MyTest(unittest.TestCase):

    def test01(self):
        n, m = 11, 13
        y = 2
        self.assertEqual(differentRightmostBit(n, m), y)

    def test02(self):
        n, m = 7, 23
        y = 16
        self.assertEqual(differentRightmostBit(n, m), y)

    def test03(self):
        n, m = 1, 0
        y = 1
        self.assertEqual(differentRightmostBit(n, m), y)

    def test04(self):
        n, m = 64, 65
        y = 1
        self.assertEqual(differentRightmostBit(n, m), y)

    def test05(self):
        n, m = 1073741823, 1071513599
        y = 131072
        self.assertEqual(differentRightmostBit(n, m), y)

    def test06(self):
        n, m = 42, 22
        y = 4
        self.assertEqual(differentRightmostBit(n, m), y)


if __name__ == '__main__':
    unittest.main()
