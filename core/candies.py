import unittest


def candies(n, m):
    return m // n * n


class MyTest(unittest.TestCase):

    def test01(self):
        n, m = 3, 10
        y = 9
        self.assertEqual(candies(n, m), y)

    def test02(self):
        n, m = 1, 2
        y = 2
        self.assertEqual(candies(n, m), y)

    def test03(self):
        n, m = 10, 5
        y = 0
        self.assertEqual(candies(n, m), y)

    def test04(self):
        n, m = 4, 4
        y = 4
        self.assertEqual(candies(n, m), y)


if __name__ == '__main__':
    unittest.main()
