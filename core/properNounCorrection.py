import unittest


def properNounCorrection(noun):
    return noun[0].upper() + noun[1:].lower()


class MyTest(unittest.TestCase):

    def test01(self):
        noun = 'pARiS'
        y = 'Paris'
        self.assertEqual(properNounCorrection(noun), y)

    def test02(self):
        noun = 'John'
        y = 'John'
        self.assertEqual(properNounCorrection(noun), y)

    def test03(self):
        noun = 'mary'
        y = 'Mary'
        self.assertEqual(properNounCorrection(noun), y)


if __name__ == '__main__':
    unittest.main()
