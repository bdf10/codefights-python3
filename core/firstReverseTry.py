import unittest


def firstReverseTry(l):
    try:
        l[0], l[-1] = l[-1], l[0]
        return l
    except IndexError:
        return l


class MyTest(unittest.TestCase):

    def test01(self):
        l = [1, 2, 3, 4, 5]
        y = [5, 2, 3, 4, 1]
        self.assertEqual(firstReverseTry(l), y)

    def test02(self):
        l = []
        y = []
        self.assertEqual(firstReverseTry(l), y)

    def test03(self):
        l = [239]
        y = [239]
        self.assertEqual(firstReverseTry(l), y)

    def test04(self):
        l = [23, 54, 12, 3, 4, 56, 23, 12, 5, 324]
        y = [324, 54, 12, 3, 4, 56, 23, 12, 5, 23]
        self.assertEqual(firstReverseTry(l), y)

    def test05(self):
        l = [-1, 1]
        y = [1, -1]
        self.assertEqual(firstReverseTry(l), y)

    def test06(self):
        l = [88, -101, -310, 818, 747, -888, -183, -687, -723, 188, -611, 677, -597, 293, -295, -162, -265, 368, 346, 81, -831, 198, -94, 685, -
             434, -241, -566, -397, 501, -183, 366, -669, 96, -592, 358, 598, 444, -929, 769, -361, -754, 218, -464, 332, 893, 422, -192, -287, -850, 543]
        y = [543, -101, -310, 818, 747, -888, -183, -687, -723, 188, -611, 677, -597, 293, -295, -162, -265, 368, 346, 81, -831, 198, -94, 685, -
             434, -241, -566, -397, 501, -183, 366, -669, 96, -592, 358, 598, 444, -929, 769, -361, -754, 218, -464, 332, 893, 422, -192, -287, -850, 88]
        self.assertEqual(firstReverseTry(l), y)


if __name__ == '__main__':
    unittest.main()
