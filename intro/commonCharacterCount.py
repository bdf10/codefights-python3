import unittest


def commonCharacterCount(s1, s2):
    return sum(min(s1.count(c), s2.count(c)) for c in set(s1))


class MyTest(unittest.TestCase):

    def test01(self):
        s1 = 'aabcc'
        s2 = 'adcaa'
        y = 3
        self.assertEqual(commonCharacterCount(s1, s2), y)

    def test02(self):
        s1 = 'zzzz'
        s2 = 'zzzzzzz'
        y = 4
        self.assertEqual(commonCharacterCount(s1, s2), y)

    def test03(self):
        s1 = 'abca'
        s2 = 'xyzbac'
        y = 3
        self.assertEqual(commonCharacterCount(s1, s2), y)

    def test04(self):
        s1 = 'a'
        s2 = 'b'
        y = 0
        self.assertEqual(commonCharacterCount(s1, s2), y)

    def test05(self):
        s1 = 'a'
        s2 = 'aaa'
        y = 1
        self.assertEqual(commonCharacterCount(s1, s2), y)




if __name__ == '__main__':
    unittest.main()
