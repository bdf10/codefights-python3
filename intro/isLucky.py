import unittest


def isLucky(n):
    s = str(n)
    half = len(s) // 2
    l1 = s[:half]
    l2 = s[half:]
    return sum(int(d) for d in l1) == sum(int(d) for d in l2)


class MyTest(unittest.TestCase):

    def test01(self):
        x = 1230
        y = True
        self.assertEqual(isLucky(x), y)

    def test02(self):
        x = 239017
        y = False
        self.assertEqual(isLucky(x), y)

    def test03(self):
        x = 134008
        y = True
        self.assertEqual(isLucky(x), y)

    def test04(self):
        x = 10
        y = False
        self.assertEqual(isLucky(x), y)

    def test05(self):
        x = 11
        y = True
        self.assertEqual(isLucky(x), y)

    def test06(self):
        x = 1010
        y = True
        self.assertEqual(isLucky(x), y)

    def test07(self):
        x = 261534
        y = False
        self.assertEqual(isLucky(x), y)

    def test08(self):
        x = 100000
        y = False
        self.assertEqual(isLucky(x), y)

    def test09(self):
        x = 999999
        y = True
        self.assertEqual(isLucky(x), y)

    def test10(self):
        x = 123321
        y = True
        self.assertEqual(isLucky(x), y)



if __name__ == '__main__':
    unittest.main()
