from itertools import zip_longest
from string import ascii_lowercase as letters
import unittest


def alphabeticShift(inputString):
    d = dict(zip_longest(letters, letters[1:], fillvalue='a'))
    return ''.join(d[c] for c in inputString)


class MyTest(unittest.TestCase):

    def test01(self):
        inputString = 'crazy'
        y = 'dsbaz'
        self.assertEqual(alphabeticShift(inputString), y)

    def test02(self):
        inputString = 'z'
        y = 'a'
        self.assertEqual(alphabeticShift(inputString), y)

    def test03(self):
        inputString = 'aaaabbbccd'
        y = 'bbbbcccdde'
        self.assertEqual(alphabeticShift(inputString), y)

    def test04(self):
        inputString = 'fuzzy'
        y = 'gvaaz'
        self.assertEqual(alphabeticShift(inputString), y)

    def test05(self):
        inputString = 'codefights'
        y = 'dpefgjhiut'
        self.assertEqual(alphabeticShift(inputString), y)


if __name__ == '__main__':
    unittest.main()
