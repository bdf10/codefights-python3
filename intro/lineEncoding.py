from itertools import groupby
import unittest


def lineEncoding(s):
    encode = lambda t: (str(len(list(t[1]))) + t[0]).lstrip('1')
    return ''.join(map(encode, groupby(s)))


class MyTest(unittest.TestCase):

    def test01(self):
        s = 'aabbbc'
        y = '2a3bc'
        self.assertEqual(lineEncoding(s), y)

    def test02(self):
        s = 'abbcabb'
        y = 'a2bca2b'
        self.assertEqual(lineEncoding(s), y)

    def test03(self):
        s = 'abcd'
        y = 'abcd'
        self.assertEqual(lineEncoding(s), y)


if __name__ == '__main__':
    unittest.main()
