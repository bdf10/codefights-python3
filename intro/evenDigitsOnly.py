import unittest


def evenDigitsOnly(n):
    return all((int(a) + 1) % 2 for a in str(n))


class MyTest(unittest.TestCase):

    def test01(self):
        n = 248622
        y = True
        self.assertEqual(evenDigitsOnly(n), y)

    def test02(self):
        n = 642386
        y = False
        self.assertEqual(evenDigitsOnly(n), y)

    def test03(self):
        n = 248842
        y = True
        self.assertEqual(evenDigitsOnly(n), y)

    def test04(self):
        n = 1
        y = False
        self.assertEqual(evenDigitsOnly(n), y)

    def test05(self):
        n = 8
        y = True
        self.assertEqual(evenDigitsOnly(n), y)

    def test06(self):
        n = 2462487
        y = False
        self.assertEqual(evenDigitsOnly(n), y)

    def test07(self):
        n = 468402800
        y = True
        self.assertEqual(evenDigitsOnly(n), y)

    def test08(self):
        n = 2468428
        y = True
        self.assertEqual(evenDigitsOnly(n), y)

    def test09(self):
        n = 5468428
        y = False
        self.assertEqual(evenDigitsOnly(n), y)

    def test10(self):
        n = 7468428
        y = False
        self.assertEqual(evenDigitsOnly(n), y)


if __name__ == '__main__':
    unittest.main()
