import unittest


def arrayChange(l):
    moves = 0
    n = len(l)
    for i in range(n-1):
        x, y = l[i], l[i + 1]
        if x >= y:
            l[i + 1] = x + 1
            moves += x - y + 1
    return moves


class MyTest(unittest.TestCase):

    def test01(self):
        l = [1, 1, 1]
        y = 3
        self.assertEqual(arrayChange(l), y)

    def test02(self):
        l = [-1000, 0, -2, 0]
        y = 5
        self.assertEqual(arrayChange(l), y)

    def test03(self):
        l = [2, 1, 10, 1]
        y = 12
        self.assertEqual(arrayChange(l), y)

    def test04(self):
        l = [2, 3, 3, 5, 5, 5, 4, 12, 12, 10, 15]
        y = 13
        self.assertEqual(arrayChange(l), y)


if __name__ == '__main__':
    unittest.main()
