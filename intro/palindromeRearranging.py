import unittest


def palindromeRearranging(s):
    n = len(s)
    numevens = sum(bool((s.count(c) + 1) % 2) for c in set(s))
    numodds = sum(bool(s.count(c) % 2) for c in set(s))
    if n % 2:
        return numodds == 1
    return numodds == 0


class MyTest(unittest.TestCase):

    def test01(self):
        s = 'aabb'
        y = True
        self.assertEqual(palindromeRearranging(s), y)
        pass

    def test02(self):
        s = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaabc'
        y = False
        self.assertEqual(palindromeRearranging(s), y)
        pass

    def test03(self):
        s = 'abbcabb'
        y = True
        self.assertEqual(palindromeRearranging(s), y)
        pass

    def test04(self):
        s = 'zyyzzzzz'
        y = True
        self.assertEqual(palindromeRearranging(s), y)
        pass

    def test05(self):
        s = 'z'
        y = True
        self.assertEqual(palindromeRearranging(s), y)
        pass

    def test06(self):
        s = 'zaa'
        y = True
        self.assertEqual(palindromeRearranging(s), y)
        pass

    def test07(self):
        s = 'abca'
        y = False
        self.assertEqual(palindromeRearranging(s), y)


if __name__ == '__main__':
    unittest.main()
