import unittest


def zeroindex(l):
    try:
        return l.index(0)
    except ValueError:
        return None


def matrixElementsSum(A):
    n = len(A[0])
    AT = [[row[i] for row in A] for i in range(n)]
    return sum(sum(col[:zeroindex(col)]) for col in AT)


class MyTest(unittest.TestCase):

    def test01(self):
        self.assertEqual(matrixElementsSum(
            [[0, 1, 1, 2], [0, 5, 0, 0], [2, 0, 3, 3]]), 9)

    def test02(self):
        self.assertEqual(matrixElementsSum(
            [[1, 1, 1, 0], [0, 5, 0, 1], [2, 1, 3, 10]]), 9)

    def test03(self):
        self.assertEqual(matrixElementsSum(
            [[1, 1, 1], [2, 2, 2], [3, 3, 3]]), 18)

    def test04(self):
        self.assertEqual(matrixElementsSum([[0]]), 0)


if __name__ == '__main__':
    unittest.main()
