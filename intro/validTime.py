import unittest


def validTime(time):
    h, m = time.split(':')
    return 0 <= int(h) <= 23 and 0 <= int(m) <= 59


class MyTest(unittest.TestCase):

    def test01(self):
        time = '13:58'
        y = True
        self.assertEqual(validTime(time), y)

    def test02(self):
        time = '25:51'
        y = False
        self.assertEqual(validTime(time), y)

    def test03(self):
        time = '02:76'
        y = False
        self.assertEqual(validTime(time), y)

    def test04(self):
        time = '24:00'
        y = False
        self.assertEqual(validTime(time), y)


if __name__ == '__main__':
    unittest.main()
