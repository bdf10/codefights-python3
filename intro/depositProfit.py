from itertools import count
import unittest


def depositProfit(deposit, rate, threshold):
    for t in count(0):
        if deposit >= threshold:
            return t
        deposit = (1 + rate / 100) * deposit


class MyTest(unittest.TestCase):

    def test01(self):
        deposit, rate, threshold = 100, 20, 170
        y = 3
        self.assertEqual(depositProfit(deposit, rate, threshold), y)

    def test02(self):
        deposit, rate, threshold = 100, 1, 101
        y = 1
        self.assertEqual(depositProfit(deposit, rate, threshold), y)

    def test03(self):
        deposit, rate, threshold = 1, 100, 64
        y = 6
        self.assertEqual(depositProfit(deposit, rate, threshold), y)


if __name__ == '__main__':
    unittest.main()
