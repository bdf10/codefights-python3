import unittest


def absSumMin(L, x):
    return sum(abs(l - x) for l in L)


def absoluteValuesSumMinimization(L):
    d = {l: absSumMin(L, l) for l in L}
    m = min(d.values())
    return min(l for l in d if d[l] == m)


class MyTest(unittest.TestCase):

    def test01(self):
        a = [2, 4, 7]
        y = 4
        self.assertEqual(absoluteValuesSumMinimization(a), y)

    def test02(self):
        a = [1, 1, 3, 4]
        y = 1
        self.assertEqual(absoluteValuesSumMinimization(a), y)

    def test03(self):
        a = [23]
        y = 23
        self.assertEqual(absoluteValuesSumMinimization(a), y)

    def test04(self):
        a = [-10, -10, -10, -10, -10, -9, -9, -9, -8, -8, -7, -6, -5, -4, -3, -2, -1, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
             16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50]
        y = 15
        self.assertEqual(absoluteValuesSumMinimization(a), y)

    def test05(self):
        a = [-4, -1]
        y = -4
        self.assertEqual(absoluteValuesSumMinimization(a), y)

    def test06(self):
        a = [0, 7, 9]
        y = 7
        self.assertEqual(absoluteValuesSumMinimization(a), y)

    def test07(self):
        a = [-1000000, -10000, -10000, -1000, -100, -10, -
             1, 0, 1, 10, 100, 1000, 10000, 100000, 1000000]
        y = 0
        self.assertEqual(absoluteValuesSumMinimization(a), y)


if __name__ == '__main__':
    unittest.main()
