import unittest


def extractEachKth(inputArray, k):
    return [x for i, x in enumerate(inputArray) if (i + 1) % k != 0]


class MyTest(unittest.TestCase):

    def test01(self):
        inputArray, k = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3
        y = [1, 2, 4, 5, 7, 8, 10]
        self.assertEqual(extractEachKth(inputArray, k), y)

    def test02(self):
        inputArray, k = [1, 1, 1, 1, 1], 1
        y = []
        self.assertEqual(extractEachKth(inputArray, k), y)

    def test03(self):
        inputArray, k = [1, 2, 1, 2, 1, 2, 1, 2], 2
        y = [1, 1, 1, 1]
        self.assertEqual(extractEachKth(inputArray, k), y)


if __name__ == '__main__':
    unittest.main()
