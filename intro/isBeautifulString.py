from string import ascii_lowercase as letters
import unittest


def isBeautifulString(st):
    z = zip(letters, letters[1:])
    return all(st.count(l2) <= st.count(l1) for l1, l2 in z)


class MyTest(unittest.TestCase):

    def test01(self):
        st = 'bbbaacdafe'
        y = True
        self.assertEqual(isBeautifulString(st), y)

    def test02(self):
        st = 'aabbb'
        y = False
        self.assertEqual(isBeautifulString(st), y)

    def test03(self):
        st = 'bbc'
        y = False
        self.assertEqual(isBeautifulString(st), y)

    def test04(self):
        st = 'bbbaa'
        y = False
        self.assertEqual(isBeautifulString(st), y)

    def test05(self):
        st = 'abcdefghijklmnopqrstuvwxyzz'
        y = False
        self.assertEqual(isBeautifulString(st), y)

    def test06(self):
        st = 'abcdefghijklmnopqrstuvwxyz'
        y = True
        self.assertEqual(isBeautifulString(st), y)

    def test07(self):
        st = 'abcdefghijklmnopqrstuvwxyzqwertuiopasdfghjklxcvbnm'
        y = True
        self.assertEqual(isBeautifulString(st), y)

    def test08(self):
        st = 'fyudhrygiuhdfeis'
        y = False
        self.assertEqual(isBeautifulString(st), y)

    def test09(self):
        st = 'zaa'
        y = False
        self.assertEqual(isBeautifulString(st), y)

    def test10(self):
        st = 'zyy'
        y = False
        self.assertEqual(isBeautifulString(st), y)


if __name__ == '__main__':
    unittest.main()
