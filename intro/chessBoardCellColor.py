from string import ascii_uppercase as letters
import unittest


def cell2pair(cell):
    d = {l: i + 1 for i, l in enumerate(letters)}
    l, i = cell
    return (d[l], int(i))


def chessBoardCellColor(cell1, cell2):
    t1, t2 = [cell2pair(cell) for cell in (cell1, cell2)]
    return sum(t1) % 2 == sum(t2) % 2


class MyTest(unittest.TestCase):

    def test01(self):
        cell1, cell2 = 'A1', 'C3'
        y = True
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)

    def test02(self):
        cell1, cell2 = 'A1', 'H3'
        y = False
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)

    def test03(self):
        cell1, cell2 = 'A1', 'A2'
        y = False
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)

    def test04(self):
        cell1, cell2 = 'A1', 'B2'
        y = True
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)

    def test05(self):
        cell1, cell2 = 'B3', 'H8'
        y = False
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)

    def test06(self):
        cell1, cell2 = 'C3', 'B5'
        y = False
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)

    def test07(self):
        cell1, cell2 = 'G5', 'E7'
        y = True
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)

    def test08(self):
        cell1, cell2 = 'C8', 'H8'
        y = False
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)

    def test09(self):
        cell1, cell2 = 'D2', 'D2'
        y = True
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)

    def test10(self):
        cell1, cell2 = 'A2', 'A5'
        y = False
        self.assertEqual(chessBoardCellColor(cell1, cell2), y)


if __name__ == '__main__':
    unittest.main()
