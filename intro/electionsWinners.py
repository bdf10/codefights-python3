import unittest


def electionsWinners(votes, k):
    m = max(votes)
    if k == 0:
        return int(votes.count(m) == 1)
    return sum(bool(v + k > m) for v in votes)


class MyTest(unittest.TestCase):

    def test01(self):
        votes, k = [2, 3, 5, 2], 3
        y = 2
        self.assertEqual(electionsWinners(votes, k), y)

    def test02(self):
        votes, k = [1, 3, 3, 1, 1], 0
        y = 0
        self.assertEqual(electionsWinners(votes, k), y)

    def test03(self):
        votes, k = [5, 1, 3, 4, 1], 0
        y = 1
        self.assertEqual(electionsWinners(votes, k), y)

    def test04(self):
        votes, k = [1, 1, 1, 1], 1
        y = 4
        self.assertEqual(electionsWinners(votes, k), y)

    def test05(self):
        votes, k = [1, 1, 1, 1], 0
        y = 0
        self.assertEqual(electionsWinners(votes, k), y)

    def test06(self):
        votes, k = [3, 1, 1, 3, 1], 2
        y = 2
        self.assertEqual(electionsWinners(votes, k), y)


if __name__ == '__main__':
    unittest.main()
