import unittest


def areEquallyStrong(yourLeft, yourRight, friendsLeft, friendsRight):
    return set([yourLeft, yourRight]) == set([friendsLeft, friendsRight])


class MyTest(unittest.TestCase):

    def test01(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 10, 15, 15, 10
        y = True
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test02(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 15, 10, 15, 10
        y = True
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test03(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 15, 10, 15, 9
        y = False
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test04(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 10, 5, 5, 10
        y = True
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test05(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 10, 15, 5, 20
        y = False
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test06(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 10, 20, 10, 20
        y = True
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test07(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 5, 20, 20, 5
        y = True
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test08(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 20, 15, 5, 20
        y = False
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test09(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 5, 10, 5, 10
        y = True
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test10(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 1, 10, 10, 0
        y = False
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test11(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 5, 5, 10, 10
        y = False
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test12(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 10, 5, 10, 6
        y = False
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)
        pass

    def test13(self):
        yourLeft, yourRight, friendsLeft, friendsRight = 1, 1, 1, 1
        y = True
        self.assertEqual(areEquallyStrong(
            yourLeft, yourRight, friendsLeft, friendsRight), y)


if __name__ == '__main__':
    unittest.main()
