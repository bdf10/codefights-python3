import unittest


def add(param1, param2):
    return param1 + param2


class MyTest(unittest.TestCase):

    def test1(self):
        self.assertEqual(add(1, 2), 3)

    def test2(self):
        self.assertEqual(add(0, 1000), 1000)

    def test3(self):
        self.assertEqual(add(2, -39), -37)

    def test4(self):
        self.assertEqual(add(99, 100), 199)

    def test5(self):
        self.assertEqual(add(-100, 100), 0)

    def test6(self):
        self.assertEqual(add(-1000, -1000), -2000)


if __name__ == '__main__':
    unittest.main()
