import unittest


def isDigit(s):
    return s.isdigit()


class MyTest(unittest.TestCase):

    def test01(self):
        s = '0'
        y = True
        self.assertEqual(isDigit(s), y)

    def test02(self):
        s = '-'
        y = False
        self.assertEqual(isDigit(s), y)

    def test03(self):
        s = 'O'
        y = False
        self.assertEqual(isDigit(s), y)

    def test04(self):
        s = '1'
        y = True
        self.assertEqual(isDigit(s), y)

    def test05(self):
        s = '2'
        y = True
        self.assertEqual(isDigit(s), y)

    def test06(self):
        s = '!'
        y = False
        self.assertEqual(isDigit(s), y)

    def test07(self):
        s = '@'
        y = False
        self.assertEqual(isDigit(s), y)

    def test08(self):
        s = '+'
        y = False
        self.assertEqual(isDigit(s), y)

    def test09(self):
        s = '6'
        y = True
        self.assertEqual(isDigit(s), y)

    def test10(self):
        s = '('
        y = False
        self.assertEqual(isDigit(s), y)

    def test10(self):
        s = ')'
        y = False
        self.assertEqual(isDigit(s), y)


if __name__ == '__main__':
    unittest.main()
