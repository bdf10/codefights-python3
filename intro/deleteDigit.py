import unittest


def popdigit(n, k):
    return int(''.join(digit for i, digit in enumerate(str(n)) if i != k))


def deleteDigit(n):
    ndigits = len(str(n))
    return max(popdigit(n, i) for i in range(ndigits))


class MyTest(unittest.TestCase):

    def test01(self):
        n = 152
        y = 52
        self.assertEqual(deleteDigit(n), y)

    def test02(self):
        n = 1001
        y = 101
        self.assertEqual(deleteDigit(n), y)

    def test03(self):
        n = 10
        y = 1
        self.assertEqual(deleteDigit(n), y)

    def test04(self):
        n = 222219
        y = 22229
        self.assertEqual(deleteDigit(n), y)


if __name__ == '__main__':
    unittest.main()
