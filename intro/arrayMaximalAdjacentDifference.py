import unittest


def arrayMaximalAdjacentDifference(inputArray):
    return max(abs(x - y) for x, y in zip(inputArray, inputArray[1:]))


class MyTest(unittest.TestCase):

    def test01(self):
        inputArray = [2, 4, 1, 0]
        y = 3
        self.assertEqual(arrayMaximalAdjacentDifference(inputArray), y)
        pass

    def test02(self):
        inputArray = [1, 1, 1, 1]
        y = 0
        self.assertEqual(arrayMaximalAdjacentDifference(inputArray), y)
        pass

    def test03(self):
        inputArray = [-1, 4, 10, 3, -2]
        y = 7
        self.assertEqual(arrayMaximalAdjacentDifference(inputArray), y)
        pass

if __name__ == '__main__':
    unittest.main()
