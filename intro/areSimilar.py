import unittest


def areSimilar(a, b):
    badpairs = [(x, y) for x, y in zip(a, b) if x != y]
    if not badpairs:
        return True
    if len(badpairs) != 2:
        return False
    (a1, b1), (a2, b2) = badpairs
    return (a1, a2) == (b2, b1)


class MyTest(unittest.TestCase):

    def test01(self):
        a, b = [1, 2, 3], [1, 2, 3]
        y = True
        self.assertEqual(areSimilar(a, b), y)

    def test02(self):
        a, b = [1, 2, 3], [2, 1, 3]
        y = True
        self.assertEqual(areSimilar(a, b), y)

    def test03(self):
        a, b = [1, 2, 2], [2, 1, 1]
        y = False
        self.assertEqual(areSimilar(a, b), y)

    def test04(self):
        a, b = [1, 1, 4], [1, 2, 3]
        y = False
        self.assertEqual(areSimilar(a, b), y)

    def test05(self):
        a, b = [1, 2, 3], [1, 10, 2]
        y = False
        self.assertEqual(areSimilar(a, b), y)

    def test06(self):
        a, b = [2, 3, 1], [1, 3, 2]
        y = True
        self.assertEqual(areSimilar(a, b), y)

    def test07(self):
        a, b = [2, 3, 9], [10, 3, 2]
        y = False
        self.assertEqual(areSimilar(a, b), y)

    def test08(self):
        a, b = [4, 6, 3], [3, 4, 6]
        y = False
        self.assertEqual(areSimilar(a, b), y)

    def test09(self):
        a = [832, 998, 148, 570, 533, 561, 894, 147, 455, 279]
        b = [832, 998, 148, 570, 533, 561, 455, 147, 894, 279]
        y = True
        self.assertEqual(areSimilar(a, b), y)

    def test10(self):
        a = [832, 998, 148, 570, 533, 561, 894, 147, 455, 279]
        b = [832, 570, 148, 998, 533, 561, 455, 147, 894, 279]
        y = False
        self.assertEqual(areSimilar(a, b), y)


if __name__ == '__main__':
    unittest.main()
