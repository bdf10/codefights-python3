import unittest


def mygen(names):
    d = {}
    for name in names:
        if not name in d:
            d[name] = 0
            yield name
        else:
            d[name] += 1
            n = d[name]
            while name + '({})'.format(n) in d:
                n += 1
            newname = name + '({})'.format(n)
            d[newname] = 0
            yield newname


def fileNaming(names):
    return list(mygen(names))


class MyTest(unittest.TestCase):

    def test01(self):
        names = ['doc', 'doc', 'image', 'doc(1)', 'doc']
        y = ['doc', 'doc(1)', 'image', 'doc(1)(1)', 'doc(2)']
        self.assertEqual(fileNaming(names), y)

    def test02(self):
        names = ['a(1)', 'a(6)', 'a', 'a', 'a', 'a',
                 'a', 'a', 'a', 'a', 'a', 'a']
        y = ['a(1)', 'a(6)', 'a', 'a(2)', 'a(3)', 'a(4)',
             'a(5)', 'a(7)', 'a(8)', 'a(9)', 'a(10)', 'a(11)']
        self.assertEqual(fileNaming(names), y)

    def test03(self):
        names = ['dd', 'dd(1)', 'dd(2)', 'dd', 'dd(1)',
                 'dd(1)(2)', 'dd(1)(1)', 'dd', 'dd(1)']
        y = ['dd', 'dd(1)', 'dd(2)', 'dd(3)', 'dd(1)(1)',
             'dd(1)(2)', 'dd(1)(1)(1)', 'dd(4)', 'dd(1)(3)']
        self.assertEqual(fileNaming(names), y)


if __name__ == '__main__':
    unittest.main()
