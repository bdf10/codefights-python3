import unittest

def is_increasing(l):
    return all(a < b for a, b in zip(l, l[1:]))


def almostIncreasingSequence(l):
    for i, (a, b) in enumerate(zip(l, l[1:])):
        if a >= b:
            break
    else:
        return True
    return is_increasing(l[:i] + l[i+1:]) or is_increasing(l[:i+1] + l[i+2:])


class MyTest(unittest.TestCase):

    def test01(self):
        self.assertEqual(almostIncreasingSequence([1, 3, 2, 1]), False)

    def test02(self):
        self.assertEqual(almostIncreasingSequence([1, 3, 2]), True)

    def test03(self):
        self.assertEqual(almostIncreasingSequence([1, 2, 1, 2]), False)

    def test04(self):
        self.assertEqual(almostIncreasingSequence([1, 4, 10, 4, 2]), False)

    def test05(self):
        self.assertEqual(almostIncreasingSequence([10, 1, 2, 3, 4, 5]), True)

    def test06(self):
        self.assertEqual(almostIncreasingSequence([1, 1, 1, 2, 3]), False)

    def test07(self):
        self.assertEqual(almostIncreasingSequence([0, -2, 5, 6]), True)

    def test08(self):
        self.assertEqual(almostIncreasingSequence(
            [1, 2, 3, 4, 5, 3, 5, 6]), False)

    def test09(self):
        self.assertEqual(almostIncreasingSequence(
            [40, 50, 60, 10, 20, 30]), False)

    def test10(self):
        self.assertEqual(almostIncreasingSequence([1, 1]), True)

    def test11(self):
        self.assertEqual(almostIncreasingSequence([1, 2, 5, 3, 5]), True)

    def test12(self):
        self.assertEqual(almostIncreasingSequence([1, 2, 5, 5, 5]), False)

    def test13(self):
        self.assertEqual(almostIncreasingSequence(
            [10, 1, 2, 3, 4, 5, 6, 1]), False)

    def test14(self):
        self.assertEqual(almostIncreasingSequence([1, 2, 3, 4, 3, 6]), True)

    def test15(self):
        self.assertEqual(almostIncreasingSequence(
            [1, 2, 3, 4, 99, 5, 6]), True)

    def test16(self):
        self.assertEqual(almostIncreasingSequence(
            [123, -17, -5, 1, 2, 3, 12, 43, 45]), True)

    def test17(self):
        self.assertEqual(almostIncreasingSequence([3, 5, 67, 98, 3]), True)


if __name__ == '__main__':
    unittest.main()
