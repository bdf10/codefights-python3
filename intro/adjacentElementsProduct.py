import unittest


def adjacentElementsProduct(inputArray):
    product = inputArray[0] * inputArray[1]
    for a, b in zip(inputArray[1:], inputArray[2:]):
        if a * b > product:
            product = a * b
    return product


class MyTest(unittest.TestCase):

    def test01(self):
        self.assertEqual(adjacentElementsProduct([3, 6, -2, -5, 7, 3]), 21)

    def test02(self):
        self.assertEqual(adjacentElementsProduct([-1, -2]), 2)

    def test03(self):
        self.assertEqual(adjacentElementsProduct([5, 1, 2, 3, 1, 4]), 6)

    def test04(self):
        self.assertEqual(adjacentElementsProduct([1, 2, 3, 0]), 6)

    def test05(self):
        self.assertEqual(adjacentElementsProduct(
            [9, 5, 10, 2, 24, -1, -48]), 50)

    def test06(self):
        self.assertEqual(adjacentElementsProduct([5, 6, -4, 2, 3, 2, -23]), 30)

    def test07(self):
        self.assertEqual(adjacentElementsProduct([4, 1, 2, 3, 1, 5]), 6)

    def test08(self):
        self.assertEqual(adjacentElementsProduct([-23, 4, -3, 8, -12]), -12)

    def test09(self):
        self.assertEqual(adjacentElementsProduct([1, 0, 1, 0, 1000]), 0)


if __name__ == '__main__':
    unittest.main()
