import unittest


def arrayReplace(inputArray, elemToReplace, substitutionElem):
    return [substitutionElem if x == elemToReplace else x for x in inputArray]


class MyTest(unittest.TestCase):

    def test01(self):
        inputArray = [1, 2, 1]
        elemToReplace = 1
        substitutionElem = 3
        y = [3, 2, 3]
        self.assertEqual(arrayReplace(inputArray, elemToReplace, substitutionElem), y)

    def test02(self):
        inputArray = [1, 2, 3, 4, 5]
        elemToReplace = 3
        substitutionElem = 0
        y = [1, 2, 0, 4, 5]
        self.assertEqual(arrayReplace(inputArray, elemToReplace, substitutionElem), y)

    def test03(self):
        inputArray = [1, 1, 1]
        elemToReplace = 1
        substitutionElem = 10
        y = [10, 10, 10]
        self.assertEqual(arrayReplace(inputArray, elemToReplace, substitutionElem), y)


if __name__ == '__main__':
    unittest.main()
