import unittest


def sortByHeight(a):
    s = sorted(x for x in a if x != -1)
    for i, x in enumerate(a):
        if x == -1:
            s.insert(i, -1)
    return s


class MyTest(unittest.TestCase):

    def test01(self):
        x = [-1, 150, 190, 170, -1, -1, 160, 180]
        y = [-1, 150, 160, 170, -1, -1, 180, 190]
        self.assertEqual(sortByHeight(x), y)

    def test02(self):
        x = [-1, -1, -1, -1, -1]
        y = [-1, -1, -1, -1, -1]
        self.assertEqual(sortByHeight(x), y)

    def test03(self):
        x = [4, 2, 9, 11, 2, 16]
        y = [2, 2, 4, 9, 11, 16]
        self.assertEqual(sortByHeight(x), y)


if __name__ == '__main__':
    unittest.main()
