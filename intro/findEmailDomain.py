import unittest


def findEmailDomain(address):
    _, domain = address.rsplit('@', 1)
    return domain


class MyTest(unittest.TestCase):

    def test01(self):
        address = 'prettyandsimple@example.com'
        y = 'example.com'
        self.assertEqual(findEmailDomain(address), y)

    def test02(self):
        address = '<>[]:,;@\"!#$%&*+-/=?^_{}| ~.a\"@example.org'
        y = 'example.org'
        self.assertEqual(findEmailDomain(address), y)

    def test03(self):
        address = 'someaddress@yandex.ru'
        y = 'yandex.ru'
        self.assertEqual(findEmailDomain(address), y)

    def test04(self):
        address = '\" \"@xample.org'
        y = 'xample.org'
        self.assertEqual(findEmailDomain(address), y)


if __name__ == '__main__':
    unittest.main()
