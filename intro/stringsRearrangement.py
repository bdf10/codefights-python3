from itertools import permutations, zip_longest
import unittest


def diffbyone(s1, s2):
    return sum(bool(c1 != c2) for c1, c2 in zip_longest(s1, s2)) == 1


def stringsRearrangement(l):
    for p in permutations(l):
        if all(diffbyone(s1, s2) for s1, s2 in zip(p, p[1:])):
            return True
    return False


class MyTest(unittest.TestCase):

    def test01(self):
        l = ['aba', 'bbb', 'bab']
        y = False
        self.assertEqual(stringsRearrangement(l), y)

    def test02(self):
        l = ['ab', 'bb', 'aa']
        y = True
        self.assertEqual(stringsRearrangement(l), y)

    def test03(self):
        l = ['q', 'q']
        y = False
        self.assertEqual(stringsRearrangement(l), y)

    def test04(self):
        l = ['zzzzab', 'zzzzbb', 'zzzzaa']
        y = True
        self.assertEqual(stringsRearrangement(l), y)

    def test05(self):
        l = ['ab', 'ad', 'ef', 'eg']
        y = False
        self.assertEqual(stringsRearrangement(l), y)

    def test06(self):
        l = ['abc', 'bef', 'bcc', 'bec', 'bbc', 'bdc']
        y = True
        self.assertEqual(stringsRearrangement(l), y)

    def test07(self):
        l = ['abc', 'abx', 'axx', 'abc']
        y = False
        self.assertEqual(stringsRearrangement(l), y)

    def test08(self):
        l = ['abc', 'abx', 'axx', 'abx', 'abc']
        y = True
        self.assertEqual(stringsRearrangement(l), y)

    def test09(self):
        l = ['f', 'g', 'a', 'h']
        y = True
        self.assertEqual(stringsRearrangement(l), y)


if __name__ == '__main__':
    unittest.main()
