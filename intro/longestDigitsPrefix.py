import unittest


def longestDigitsPrefix(st):
    prefix = ''
    for s in st:
        if not s.isdigit():
            break
        prefix += s
    return prefix


class MyTest(unittest.TestCase):

    def test01(self):
        st = '123aa1'
        y = '123'
        self.assertEqual(longestDigitsPrefix(st), y)

    def test02(self):
        st = '0123456789'
        y = '0123456789'
        self.assertEqual(longestDigitsPrefix(st), y)

    def test03(self):
        st = '  3) always check for whitespaces'
        y = ''
        self.assertEqual(longestDigitsPrefix(st), y)

    def test04(self):
        st = '12abc34'
        y = '12'
        self.assertEqual(longestDigitsPrefix(st), y)

    def test05(self):
        st = 'the output is 42'
        y = ''
        self.assertEqual(longestDigitsPrefix(st), y)


if __name__ == '__main__':
    unittest.main()
