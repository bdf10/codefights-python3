from itertools import count
import unittest


def growingPlant(upSpeed, downSpeed, desiredHeight):
    height = 0
    for t in count(1):
        height += upSpeed
        if height >= desiredHeight:
            return t
        height -= downSpeed


class MyTest(unittest.TestCase):

    def test01(self):
        pSpeed, downSpeed, desiredHeight = 100, 10, 910
        y = 10
        self.assertEqual(growingPlant(pSpeed, downSpeed, desiredHeight), y)

    def test02(self):
        pSpeed, downSpeed, desiredHeight = 10, 9, 4
        y = 1
        self.assertEqual(growingPlant(pSpeed, downSpeed, desiredHeight), y)

    def test03(self):
        pSpeed, downSpeed, desiredHeight = 5, 2, 7
        y = 2
        self.assertEqual(growingPlant(pSpeed, downSpeed, desiredHeight), y)

    def test04(self):
        pSpeed, downSpeed, desiredHeight = 7, 3, 443
        y = 110
        self.assertEqual(growingPlant(pSpeed, downSpeed, desiredHeight), y)

    def test05(self):
        pSpeed, downSpeed, desiredHeight = 6, 5, 10
        y = 5
        self.assertEqual(growingPlant(pSpeed, downSpeed, desiredHeight), y)


if __name__ == '__main__':
    unittest.main()
