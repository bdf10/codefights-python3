from functools import reduce
from operator import mul
from itertools import count
import unittest


def prod(iterable):
    return reduce(mul, iterable, 1)


def digitsProduct(p):
    if p == 0:
        return 10
    q = p
    for i in (2, 3, 5, 7):
        while q % i == 0:
            q = q // i
    if q != 1:
        return -1
    for k in count(1):
        if prod(int(i) for i in str(k)) == p:
            return k


class MyTest(unittest.TestCase):

    def test01(self):
        p = 12
        y = 26
        self.assertEqual(digitsProduct(p), y)

    def test02(self):
        p = 19
        y = -1
        self.assertEqual(digitsProduct(p), y)

    def test03(self):
        p = 450
        y = 2559
        self.assertEqual(digitsProduct(p), y)

    def test04(self):
        p = 0
        y = 10
        self.assertEqual(digitsProduct(p), y)

    def test05(self):
        p = 13
        y = -1
        self.assertEqual(digitsProduct(p), y)

    def test06(self):
        p = 1
        y = 1
        self.assertEqual(digitsProduct(p), y)

    def test07(self):
        p = 243
        y = 399
        self.assertEqual(digitsProduct(p), y)

    def test08(self):
        p = 576
        y = 889
        self.assertEqual(digitsProduct(p), y)

    def test09(self):
        p = 360
        y = 589
        self.assertEqual(digitsProduct(p), y)


if __name__ == '__main__':
    unittest.main()
