import unittest


def boxBlur(image):
    blurred = []
    for r1, r2, r3, in zip(image, image[1:], image[2:]):
        row = []
        z = list(zip(r1, r2, r3))
        for c1, c2, c3 in zip(z, z[1:], z[2:]):
            x = sum(sum(c) for c in (c1, c2, c3)) // 9
            row.append(x)
        blurred.append(row)
    return blurred


class MyTest(unittest.TestCase):

    def test01(self):
        image = [[1, 1, 1], [1, 7, 1], [1, 1, 1]]
        y = [[1]]
        self.assertEqual(boxBlur(image), y)

    def test02(self):
        image = [[0, 18, 9], [27, 9, 0], [81, 63, 45]]
        y = [[28]]
        self.assertEqual(boxBlur(image), y)

    def test03(self):
        image = [[36, 0, 18, 9], [27, 54, 9, 0], [81, 63, 72, 45]]
        y = [[40, 30]]
        self.assertEqual(boxBlur(image), y)

    def test04(self):
        image = [[7, 4, 0, 1], [5, 6, 2, 2], [6, 10, 7, 8], [1, 4, 2, 0]]
        y = [[5, 4], [4, 4]]
        self.assertEqual(boxBlur(image), y)

    def test05(self):
        image = [[36, 0, 18, 9, 9, 45, 27], [27, 0, 54, 9, 0, 63, 90], [81, 63, 72, 45, 18, 27, 0], [
            0, 0, 9, 81, 27, 18, 45], [45, 45, 27, 27, 90, 81, 72], [45, 18, 9, 0, 9, 18, 45], [27, 81, 36, 63, 63, 72, 81]]
        y = [[39, 30, 26, 25, 31], [34, 37, 35, 32, 32], [
            38, 41, 44, 46, 42], [22, 24, 31, 39, 45], [37, 34, 36, 47, 59]]
        self.assertEqual(boxBlur(image), y)


if __name__ == '__main__':
    unittest.main()
