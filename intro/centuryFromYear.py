import unittest


def centuryFromYear(year):
    return int((year - 1) / 100) + 1


class MyTest(unittest.TestCase):

    def test1(self):
        self.assertEqual(centuryFromYear(1905), 20)

    def test2(self):
        self.assertEqual(centuryFromYear(1700), 17)

    def test3(self):
        self.assertEqual(centuryFromYear(1988), 20)

    def test4(self):
        self.assertEqual(centuryFromYear(2000), 20)

    def test5(self):
        self.assertEqual(centuryFromYear(2001), 21)

    def test6(self):
        self.assertEqual(centuryFromYear(200), 2)

    def test7(self):
        self.assertEqual(centuryFromYear(374), 4)

    def test8(self):
        self.assertEqual(centuryFromYear(45), 1)

    def test9(self):
        self.assertEqual(centuryFromYear(8), 1)


if __name__ == '__main__':
    unittest.main()
