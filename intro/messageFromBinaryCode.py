from itertools import zip_longest
import unittest


def grouper(st, n, fillvalue=None):
    args = [iter(st)] * n
    return [''.join(t) for t in zip_longest(*args, fillvalue=fillvalue)]


def decode(s):
    return chr(int(s, 2))


def messageFromBinaryCode(code):
    return ''.join(decode(group) for group in grouper(code, 8))


class MyTest(unittest.TestCase):

    def test01(self):
        code = '010010000110010101101100011011000110111100100001'
        y = 'Hello!'
        self.assertEqual(messageFromBinaryCode(code), y)

    def test02(self):
        code = '01001101011000010111100100100000011101000110100001100101001000000100011001101111011100100110001101100101001000000110001001100101001000000111011101101001011101000110100000100000011110010110111101110101'
        y = 'May the Force be with you'
        self.assertEqual(messageFromBinaryCode(code), y)

    def test03(self):
        code = '010110010110111101110101001000000110100001100001011001000010000001101101011001010010000001100001011101000010000001100000011010000110010101101100011011000110111100101110'
        y = 'You had me at `hello.'
        self.assertEqual(messageFromBinaryCode(code), y)


if __name__ == '__main__':
    unittest.main()
