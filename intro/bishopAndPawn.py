from string import ascii_lowercase as letters
import unittest


def cell2pair(cell):
    d = {l: i + 1 for i, l in enumerate(letters)}
    l, i = cell
    return (d[l], int(i))


def bishopAndPawn(bishop, pawn):
    b1, b2 = cell2pair(bishop)
    p1, p2 = cell2pair(pawn)
    return abs(b1 - p1) == abs(b2 - p2)


class MyTest(unittest.TestCase):

    def test01(self):
        bishop, pawn = 'a1', 'c3'
        y = True
        self.assertEqual(bishopAndPawn(bishop, pawn), y)

    def test02(self):
        bishop, pawn = 'h1', 'h3'
        y = False
        self.assertEqual(bishopAndPawn(bishop, pawn), y)

    def test03(self):
        bishop, pawn = 'a5', 'c3'
        y = True
        self.assertEqual(bishopAndPawn(bishop, pawn), y)

    def test04(self):
        bishop, pawn = 'g1', 'f3'
        y = False
        self.assertEqual(bishopAndPawn(bishop, pawn), y)

    def test05(self):
        bishop, pawn = 'e7', 'd6'
        y = True
        self.assertEqual(bishopAndPawn(bishop, pawn), y)

    def test06(self):
        bishop, pawn = 'e7', 'a3'
        y = True
        self.assertEqual(bishopAndPawn(bishop, pawn), y)

    def test07(self):
        bishop, pawn = 'e3', 'a7'
        y = True
        self.assertEqual(bishopAndPawn(bishop, pawn), y)

    def test08(self):
        bishop, pawn = 'a1', 'h8'
        y = True
        self.assertEqual(bishopAndPawn(bishop, pawn), y)

    def test09(self):
        bishop, pawn = 'a1', 'h7'
        y = False
        self.assertEqual(bishopAndPawn(bishop, pawn), y)

    def test10(self):
        bishop, pawn = 'h1', 'a8'
        y = True
        self.assertEqual(bishopAndPawn(bishop, pawn), y)


if __name__ == '__main__':
    unittest.main()
