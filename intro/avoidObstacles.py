from itertools import count
import unittest


def avoidObstacles(inputArray):
    for n in count(2):
        if all(x % n for x in inputArray):
            return n


class MyTest(unittest.TestCase):

    def test01(self):
        inputArray = [5, 3, 6, 7, 9]
        y = 4
        self.assertEqual(avoidObstacles(inputArray), y)

    def test02(self):
        inputArray = [2, 3]
        y = 4
        self.assertEqual(avoidObstacles(inputArray), y)

    def test03(self):
        inputArray = [1, 4, 10, 6, 2]
        y = 7
        self.assertEqual(avoidObstacles(inputArray), y)


if __name__ == '__main__':
    unittest.main()
