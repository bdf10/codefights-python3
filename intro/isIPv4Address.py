import unittest


def isIPv4Address(inputString):
    for i, s in enumerate(inputString.split('.')):
        try:
            n = int(s)
        except ValueError:
            return False
        if not 0 <= n <= 255:
            return False
    return i == 3


class MyTest(unittest.TestCase):

    def test01(self):
        inputString = '172.16.254.1'
        y = True
        self.assertEqual(isIPv4Address(inputString), y)

    def test02(self):
        inputString = '172.316.254.1'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test03(self):
        inputString = '.254.255.0'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test04(self):
        inputString = '1.1.1.1a'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test05(self):
        inputString = '1'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test06(self):
        inputString = '0.254.255.0'
        y = True
        self.assertEqual(isIPv4Address(inputString), y)

    def test07(self):
        inputString = '1.23.256.255.'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test08(self):
        inputString = '1.23.256..'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test09(self):
        inputString = '0..1.0'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test10(self):
        inputString = '1.1.1.1.1'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test11(self):
        inputString = '1.256.1.1'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test12(self):
        inputString = 'a0.1.1.1'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test13(self):
        inputString = '0.1.1.256'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test14(self):
        inputString = '129380129831213981.255.255.255'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test15(self):
        inputString = '255.255.255.255abcdekjhf'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)

    def test16(self):
        inputString = '7283728'
        y = False
        self.assertEqual(isIPv4Address(inputString), y)


if __name__ == '__main__':
    unittest.main()
