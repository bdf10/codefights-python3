import unittest


def minesweeper(A):
    sweep = []
    Bwidth = len(A[0]) + 2
    Btopbottom = [[None] * Bwidth]
    B = Btopbottom + [[None] + row + [None] for row in A] + Btopbottom
    for r1, r2, r3 in zip(B, B[1:], B[2:]):
        row = []
        z = list(zip(r1, r2, r3))
        for c1, c2, c3 in zip(z, z[1:], z[2:]):
            c2 = c2[0], None, c2[2]
            x = sum(bool(_) for c in (c1, c2, c3) for _ in c)
            row.append(x)
        sweep.append(row)
    return sweep


class MyTest(unittest.TestCase):

    def test01(self):
        matrix = [
            [True, False, False],
            [False, True, False],
            [False, False, False]
        ]
        y = [
            [1, 2, 1],
            [2, 1, 1],
            [1, 1, 1]
        ]
        self.assertEqual(minesweeper(matrix), y)

    def test02(self):
        matrix = [
            [False, False, False],
            [False, False, False]
        ]
        y = [
            [0, 0, 0],
            [0, 0, 0]
        ]
        self.assertEqual(minesweeper(matrix), y)

    def test03(self):
        matrix = [
            [True, False, False, True],
            [False, False, True, False],
            [True, True, False, True]
        ]
        y = [
            [0, 2, 2, 1],
            [3, 4, 3, 3],
            [1, 2, 3, 1]
        ]
        self.assertEqual(minesweeper(matrix), y)


if __name__ == '__main__':
    unittest.main()
