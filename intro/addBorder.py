import unittest


def addBorder(a):
    n = len(a[0])
    topbottom = ['*' * (n + 2)]
    return topbottom + ['*' + row + '*' for row in a] + topbottom


class MyTest(unittest.TestCase):

    def test01(self):
        x = ['abc', 'ded']
        y = ['*****', '*abc*', '*ded*', '*****']
        self.assertEqual(addBorder(x), y)

    def test02(self):
        x = ['a']
        y = ['***', '*a*', '***']
        self.assertEqual(addBorder(x), y)

    def test03(self):
        x = ['aa', '**', 'zz']
        y = ['****', '*aa*', '****', '*zz*', '****']
        self.assertEqual(addBorder(x), y)

    def test04(self):
        x = ['abcde', 'fghij', 'klmno', 'pqrst', 'uvwxy']
        y = ['*******', '*abcde*', '*fghij*', '*klmno*', '*pqrst*', '*uvwxy*', '*******']
        self.assertEqual(addBorder(x), y)

    def test05(self):
        x = ['wzy**']
        y = ['*******', '*wzy***', '*******']
        self.assertEqual(addBorder(x), y)




if __name__ == '__main__':
    unittest.main()
