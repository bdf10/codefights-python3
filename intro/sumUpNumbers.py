import string
import unittest


def sumUpNumbers(st):
    ignore = string.ascii_letters + string.punctuation
    table = str.maketrans({key: None for key in ignore})
    st = st.translate(table)
    k = 0
    for n in st.split():
        k += int(n)
    return k


class MyTest(unittest.TestCase):

    def test01(self):
        st = '2 apples, 12 oranges'
        y = 14
        self.assertEqual(sumUpNumbers(st), y)

    def test02(self):
        st = '123450'
        y = 123450
        self.assertEqual(sumUpNumbers(st), y)

    def test03(self):
        st = 'Your payment method is invalid'
        y = 0
        self.assertEqual(sumUpNumbers(st), y)


if __name__ == '__main__':
    unittest.main()
