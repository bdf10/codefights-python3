import unittest


def makeArrayConsecutive2(statues):
    m = min(statues)
    M = max(statues)
    s = set(range(m, M))
    t = set(statues)
    return len(s - t)


class MyTest(unittest.TestCase):

    def test1(self):
        self.assertEqual(makeArrayConsecutive2([6, 2, 3, 8]), 3)

    def test2(self):
        self.assertEqual(makeArrayConsecutive2([0, 3]), 2)

    def test3(self):
        self.assertEqual(makeArrayConsecutive2([5, 4, 6]), 0)

    def test4(self):
        self.assertEqual(makeArrayConsecutive2([6, 3]), 2)

    def test5(self):
        self.assertEqual(makeArrayConsecutive2([1]), 0)


if __name__ == '__main__':
    unittest.main()
