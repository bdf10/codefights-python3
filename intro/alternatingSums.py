import unittest


def alternatingSums(a):
    return [sum(a[0::2]), sum(a[1::2])]


class MyTest(unittest.TestCase):

    def test01(self):
        x = [50, 60, 60, 45, 70]
        y = [180, 105]
        self.assertEqual(alternatingSums(x), y)

    def test02(self):
        x = [100, 50]
        y = [100, 50]
        self.assertEqual(alternatingSums(x), y)

    def test03(self):
        x = [80]
        y = [80, 0]
        self.assertEqual(alternatingSums(x), y)


if __name__ == '__main__':
    unittest.main()
