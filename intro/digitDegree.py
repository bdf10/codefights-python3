from itertools import count
import unittest


def digitDegree(n):
    for k in count(0):
        st = str(n)
        if len(st) == 1:
            return k
        n = sum(int(s) for s in st)


class MyTest(unittest.TestCase):

    def test01(self):
        n = 5
        y = 0
        self.assertEqual(digitDegree(n), y)

    def test02(self):
        n = 100
        y = 1
        self.assertEqual(digitDegree(n), y)

    def test03(self):
        n = 91
        y = 2
        self.assertEqual(digitDegree(n), y)

    def test04(self):
        n = 99
        y = 2
        self.assertEqual(digitDegree(n), y)


if __name__ == '__main__':
    unittest.main()
