from string import ascii_letters as letters
import unittest


def longestWord(st):
    puncts = set(st) - set(letters) - set(' ')
    for punct in puncts:
        st = st.replace(punct, ' ')
    words = [''.join(c for c in word if c in letters) for word in st.split()]
    m = max(len(word) for word in words)
    for word in words:
        if len(word) == m:
            return word


class MyTest(unittest.TestCase):

    def test01(self):
        st = 'Ready, steady, go!'
        y = 'steady'
        self.assertEqual(longestWord(st), y)

    def test02(self):
        st = 'Ready[[[, steady, go!'
        y = 'steady'
        self.assertEqual(longestWord(st), y)

    def test03(self):
        st = 'ABCd'
        y = 'ABCd'
        self.assertEqual(longestWord(st), y)


if __name__ == '__main__':
    unittest.main()
