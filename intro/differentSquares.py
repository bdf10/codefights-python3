import unittest


def genmats(A):
    for r, s in zip(A, A[1:]):
        yield from zip(zip(r, s), zip(r[1:], s[1:]))


def differentSquares(A):
    return len(set(genmats(A)))


class MyTest(unittest.TestCase):

    def test01(self):
        A = [[1, 2, 1], [2, 2, 2], [2, 2, 2], [1, 2, 3], [2, 2, 1]]
        y = 6
        self.assertEqual(differentSquares(A), y)

    def test02(self):
        A = [[9, 9, 9, 9, 9], [9, 9, 9, 9, 9], [9, 9, 9, 9, 9],
             [9, 9, 9, 9, 9], [9, 9, 9, 9, 9], [9, 9, 9, 9, 9]]
        y = 1
        self.assertEqual(differentSquares(A), y)

    def test03(self):
        A = [[3]]
        y = 0
        self.assertEqual(differentSquares(A), y)

    def test04(self):
        A = [[3, 4, 5, 6, 7, 8, 9]]
        y = 0
        self.assertEqual(differentSquares(A), y)

    def test05(self):
        A = [[3], [4], [5], [6], [7]]
        y = 0
        self.assertEqual(differentSquares(A), y)

    def test06(self):
        A = [[2, 5, 3, 4, 3, 1, 3, 2], [4, 5, 4, 1, 2, 4, 1, 3], [1, 1, 2, 1, 4, 1, 1, 5], [1, 3, 4, 2, 3, 4, 2, 4], [
            1, 5, 5, 2, 1, 3, 1, 1], [1, 2, 3, 3, 5, 1, 2, 4], [3, 1, 4, 4, 4, 1, 5, 5], [5, 1, 3, 3, 1, 5, 3, 5], [5, 4, 4, 3, 5, 4, 4, 4]]
        y = 54
        self.assertEqual(differentSquares(A), y)

    def test07(self):
        A = [[1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 9, 9, 9, 2, 3, 9]]
        y = 0
        self.assertEqual(differentSquares(A), y)


if __name__ == '__main__':
    unittest.main()
