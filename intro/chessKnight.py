from string import ascii_lowercase as letters
from itertools import count, permutations
import unittest


def cell2pair(cell):
    d = {l: i + 1 for i, l in enumerate(letters)}
    l, i = cell
    return (d[l], int(i))


def chessKnight(cell):
    a, b = cell2pair(cell)
    nums = {1, -1, 2, -2}
    g = count(0)
    for x, y in permutations(nums, 2):
        if abs(x) != abs(y) and 1 <= a + x <= 8 and 1 <= b + y <= 8:
            next(g)
    return next(g)


class MyTest(unittest.TestCase):

    def test01(self):
        cell = 'a1'
        y = 2
        self.assertEqual(chessKnight(cell), y)

    def test02(self):
        cell = 'c2'
        y = 6
        self.assertEqual(chessKnight(cell), y)

    def test03(self):
        cell = 'd4'
        y = 8
        self.assertEqual(chessKnight(cell), y)

    def test04(self):
        cell = 'g6'
        y = 6
        self.assertEqual(chessKnight(cell), y)


if __name__ == '__main__':
    unittest.main()
