import unittest


def allLongestStrings(l):
    m = max(len(s) for s in l)
    return [s for s in l if len(s) == m]


class MyTest(unittest.TestCase):

    def test01(self):
        x = ['aba', 'aa', 'ad', 'vcd', 'aba']
        y = ['aba', 'vcd', 'aba']
        self.assertEqual(allLongestStrings(x), y)

    def test02(self):
        x = ['aa']
        y = ['aa']
        self.assertEqual(allLongestStrings(x), y)

    def test03(self):
        x = ['abc', 'eeee', 'abcd', 'dcd']
        y = ['eeee', 'abcd']
        self.assertEqual(allLongestStrings(x), y)

    def test04(self):
        x = ['a', 'abc', 'cbd', 'zzzzzz', 'a', 'abcdef', 'asasa', 'aaaaaa']
        y = ['zzzzzz', 'abcdef', 'aaaaaa']
        self.assertEqual(allLongestStrings(x), y)


if __name__ == '__main__':
    unittest.main()
