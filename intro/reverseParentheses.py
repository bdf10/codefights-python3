import unittest


def reverseFirstInner(s):
    a, b = s.split(')', 1)
    a, inner = a.rsplit('(', 1)
    return a + inner[::-1] + b


def reverseParentheses(s):
    while True:
        try:
            s = reverseFirstInner(s)
        except ValueError:
            return s


class MyTest(unittest.TestCase):

    def test01(self):
        x = 'a(bc)de'
        y = 'acbde'
        self.assertEqual(reverseParentheses(x), y)

    def test02(self):
        x = 'a(bcdefghijkl(mno)p)q'
        y = 'apmnolkjihgfedcbq'
        self.assertEqual(reverseParentheses(x), y)

    def test03(self):
        x = 'co(de(fight)s)'
        y = 'cosfighted'
        self.assertEqual(reverseParentheses(x), y)

    def test04(self):
        x = 'Code(Cha(lle)nge)'
        y = 'CodeegnlleahC'
        self.assertEqual(reverseParentheses(x), y)

    def test05(self):
        x = 'Where are the parentheses?'
        y = 'Where are the parentheses?'
        self.assertEqual(reverseParentheses(x), y)

    def test06(self):
        x = 'abc(cba)ab(bac)c'
        y = 'abcabcabcabc'
        self.assertEqual(reverseParentheses(x), y)

    def test07(self):
        x = 'The ((quick (brown) (fox) jumps over the lazy) dog)'
        y = 'The god quick nworb xof jumps over the lazy'
        self.assertEqual(reverseParentheses(x), y)


if __name__ == '__main__':
    unittest.main()
