import unittest


def firstDigit(inputString):
    for c in inputString:
        if c.isdigit():
            return c


class MyTest(unittest.TestCase):

    def test01(self):
        inputString = 'var_1__Int'
        y = '1'
        self.assertEqual(firstDigit(inputString), y)

    def test02(self):
        inputString = 'q2q-q'
        y = '2'
        self.assertEqual(firstDigit(inputString), y)

    def test03(self):
        inputString = '0ss'
        y = '0'
        self.assertEqual(firstDigit(inputString), y)


if __name__ == '__main__':
    unittest.main()
