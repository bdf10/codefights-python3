import unittest


def variableName(name):
    if all(_.isalpha() or _.isdigit() or _ == '_' for _ in name):
        return not name[0].isdigit()
    return False


class MyTest(unittest.TestCase):

    def test01(self):
        name = 'var_1__Int'
        y = True
        self.assertEqual(variableName(name), y)

    def test02(self):
        name = 'qq-q'
        y = False
        self.assertEqual(variableName(name), y)

    def test03(self):
        name = '2w2'
        y = False
        self.assertEqual(variableName(name), y)

    def test04(self):
        name = ' variable'
        y = False
        self.assertEqual(variableName(name), y)

    def test05(self):
        name = 'va[riable0'
        y = False
        self.assertEqual(variableName(name), y)

    def test06(self):
        name = 'variable0'
        y = True
        self.assertEqual(variableName(name), y)

    def test07(self):
        name = 'a'
        y = True
        self.assertEqual(variableName(name), y)

if __name__ == '__main__':
    unittest.main()
