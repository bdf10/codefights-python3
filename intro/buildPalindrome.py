import unittest


def ispalindrome(st):
    return st[::-1] == st


def buildPalindrome(st):
    post = ''
    g = iter(st)
    while not ispalindrome(st + post):
        post = next(g) + post
    return st + post


class MyTest(unittest.TestCase):

    def test01(self):
        st = 'abcdc'
        y = 'abcdcba'
        self.assertEqual(buildPalindrome(st), y)

    def test02(self):
        st = 'ababab'
        y = 'abababa'
        self.assertEqual(buildPalindrome(st), y)

    def test03(self):
        st = 'abba'
        y = 'abba'
        self.assertEqual(buildPalindrome(st), y)

    def test04(self):
        st = 'abaa'
        y = 'abaaba'
        self.assertEqual(buildPalindrome(st), y)


if __name__ == '__main__':
    unittest.main()
