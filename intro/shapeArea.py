import unittest


def shapeArea(n):
    return 4 * n * (n - 1) / 2 + 1


class MyTest(unittest.TestCase):

    def test1(self):
        self.assertEqual(shapeArea(2), 5)

    def test2(self):
        self.assertEqual(shapeArea(3), 13)

    def test3(self):
        self.assertEqual(shapeArea(1), 1)

    def test4(self):
        self.assertEqual(shapeArea(5), 41)


if __name__ == '__main__':
    unittest.main()
