import unittest


def checkPalindrome(inputString):
    return inputString[::-1] == inputString


class MyTest(unittest.TestCase):

    def test01(self):
        self.assertEqual(checkPalindrome('aabaa'), True)

    def test02(self):
        self.assertEqual(checkPalindrome('abac'), False)

    def test03(self):
        self.assertEqual(checkPalindrome('a'), True)

    def test04(self):
        self.assertEqual(checkPalindrome('az'), False)

    def test05(self):
        self.assertEqual(checkPalindrome('abacaba'), True)

    def test06(self):
        self.assertEqual(checkPalindrome('z'), True)

    def test07(self):
        self.assertEqual(checkPalindrome('aaabaaaa'), False)

    def test08(self):
        self.assertEqual(checkPalindrome('zzzazzazz'), False)

    def test09(self):
        self.assertEqual(checkPalindrome('hlbeeykoqqqqokyeeblh'), True)

    def test10(self):
        self.assertEqual(checkPalindrome('hlbeeykoqqqokyeeblh'), True)


if __name__ == '__main__':
    unittest.main()
