import unittest


def differentSymbolsNaive(s):
    return len(set(s))


class MyTest(unittest.TestCase):

    def test01(self):
        s = 'cabca'
        y = 3
        self.assertEqual(differentSymbolsNaive(s), y)

    def test02(self):
        s = 'aba'
        y = 2
        self.assertEqual(differentSymbolsNaive(s), y)

    def test03(self):
        s = 'ccccccccccc'
        y = 1
        self.assertEqual(differentSymbolsNaive(s), y)

    def test04(self):
        s = 'bcaba'
        y = 3
        self.assertEqual(differentSymbolsNaive(s), y)

    def test05(self):
        s = 'codefights'
        y = 10
        self.assertEqual(differentSymbolsNaive(s), y)


if __name__ == '__main__':
    unittest.main()
